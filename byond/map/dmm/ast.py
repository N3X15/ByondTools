from .parser import Transformer
class DMMTile:
    def __init__(self) -> None:
        self.id: str = ''
        self.atoms: List[DMMAtom] = []
class DMMTransformer(Transformer):
    def string(self, s: tuple) -> str:
        return s[0][1:-1]
        
    def float(self, n: tuple) -> str:
        return float(n[0])

    def tile(self, data: tuple) -> DMMTile:
        print('tile', repr(data))
        tile = DMMTile()
        tile.id = data[0]
        tile.atoms = data[1]
        return tile
