from typing import List, Type, Union

from byond.i18n import getTxnFor #isort: skip
t, _ = getTxnFor(__name__)

class TypePath:
    '''Represents a BYOND type path.
    
    .. note:
        This class automatically self-normalizes, unless you tell the constructor to not do so.'''

    def __init__(self, path: Union[List[str],str], normalize: bool=True) -> None:
        self.chunks: List[str] = [x for x in path.split('/') if x != '' or not normalize] if isinstance(path, str) else path

    def is_subtype_of(self, parent: 'TypePath') -> bool:
        '''Is this typepath a subtype of parent?
        
        Relies on string processing of the path, without checking for parent attribute.'''
        mylen = (self.chunks)
        plen = len(parent.chunks)
        if mylen <= plen:
            return False
        for i in range(plen):
            if self.chunks[i] != parent.chunks[i]:
                return False
        return True

    def __str__(self) -> str:
        return '/' + '/'.join(self.chunks)

    def __truediv__(self, other) -> Any:
        if isinstance(other, str):
            # path / 'butts'
            return TypePath(self.chunks+[other])
        elif isinstance(other, TypePath):
            # TypePath('/root') / 'butts'
            return TypePath(self.chunks+other.chunks)
        else:
            raise BadArgumentError('other must be a str or typepath')

    def __copy__(self):
        return self.copy()

    def copy(self):
        '''Create a shallow clone of the type path.'''
        return TypePath([x for x in self.chunks])