# BYONDTools

BYONDTools is a Python library designed to make programmatic manipulation
of [BYOND](https://byond.com) game files possible and approachable.

This site will guide you through the process.

## Features


* A full-featured and extensible DMI (DreamMaker Icon) API


* An object tree (Broken atm, undergoing rewrite)


* A full-featured DMM (DreamMaker Map) Format API (Broken, undergoing rewrite)

## Legal Notices

### Trademarks and Affiliations

**NOTE**: BYONDTools is neither created by, supported by, owned by, nor condoned by BYOND Software nor BYOND Software Staff.

All third-party trademarks (including logos and icons) referenced by BYONDTools remain the property of their respective owners.  Unless specifically identified as such, BYONDTools’ use of third-party trademarks does not indicate any relationship, sponsorship, or endorsement between BYONDTools and the owners of these trademarks.  Any references by BYONDTools to third-party trademarks is to identify corresponding party third-party goods and/or services and shall be considered nominative fair use under US trademark law.

### License

This project is available under the MIT Open Source License.

:::
