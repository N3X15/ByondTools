
import sys, argparse, os, re
from byond import ObjectTree, Map, MapRenderFlags
from byond.basetypes import Atom, PropertyFlags
from byond.map.format.dmm import DMMFormat

def _register_analyze(subp: argparse.ArgumentParser) -> None:
    _analyze = subp.add_parser('analyze', help='Generate a report of each atom on a map.  WARNING: huge')
    _analyze.add_argument('map', type=str, help='Map to analyze.', metavar='map.dmm')
    _analyze.set_defaults(cmd=cmd_analyze)

def cmd_analyze(args: argparse.ArgumentParser) -> None:
    tmpl_head = '''<html>
    <head>
        <title>BYONDTools Map Analysis :: {TITLE}</title>
    </head>
    <body>
        <h1>{TITLE}</h1>
        <ul>
            <li><a href="{ROOT}/instances/index.html">Instances</a></li>
            <li><a href="{ROOT}/index.html">Tiles</a></li>
        </ul>'''
    tmpl_footer = '''
    </body>
</html>
'''

    presentable_attributes = [
        'path',
        'id',
        'filename',
        'line'
    ]

    def MakePage(**kwargs):
        rewt = '.'
        depth = kwargs.get('depth', 0)
        if depth > 0:
            rewt = '/'.join(['..'] * depth)
        title = kwargs.get('title', 'LOL NO TITLE')
        body = kwargs.get('body', '')
        return (tmpl_head + body + tmpl_footer).replace('{TITLE}', title).replace('{ROOT}', rewt)

    if not os.path.isfile(args.project):
        print('DM Environment file {0} does not exist.'.format(args.theirs))
        sys.exit(1)

    if not os.path.isfile(args.map):
        print('Map {0} does not exist.'.format(args.theirs))
        sys.exit(1)

    tree = ObjectTree()
    tree.ProcessFilesFromDME(args.project)
    dmm = Map(tree)
    dmm.readMap(args.map)

    basedir = os.path.join(os.path.dirname(args.project), 'analysis', os.path.basename(args.map))

    if not os.path.isdir(basedir):
        os.makedirs(os.path.join(basedir, 'instances'))
        os.makedirs(os.path.join(basedir, 'tiles'))

    # Dump instances
    instance_info = {}
    for atom in dmm.instances:
        if atom.path not in instance_info:
            instance_info[atom.path] = []
        instance_info[atom.path] += [atom.ID]

        with open(os.path.join(basedir, 'instances', str(atom.ID) + '.html'), 'w') as f:
            body = '<h2>Atom Data:</h2><table class="prettytable"><thead><tr><th>Name</th><th>Value</th></tr></thead><tbody>'
            for attr in presentable_attributes:
                body += '<tr><th>{0}</th><td>{1}</td></tr>'.format(attr, getattr(atom, attr, None))
            body += '</tbody></table>'

            body += '<h2>Map-Specified Properties:</h2><table class="prettytable"><thead><tr><th>Name</th><th>Value</th></tr></thead><tbody>'
            for attr_name in atom.mapSpecified:
                body += '<tr><th>{0}</th><td>{1}</td></tr>'.format(attr_name, atom.getProperty(attr_name, None))
            body += '</tbody></table>'

            body += '<h2>All Properties:</h2><table class="prettytable"><thead><tr><th>Name</th><th>Value</th><th>File/Line</th></tr></thead><tbody>'
            for attr_name in sorted(atom.properties.keys()):
                attr = atom.properties[attr_name]
                body += '<tr><th>{0}</th><td>{1}</td><td>{2}:{3}</td></tr>'.format(attr_name, attr.value, attr.filename, attr.line)
            body += '</tbody></table>'
            f.write(MakePage(title='Instance #{0}'.format(atom.ID), depth=1, body=body))
    with open(os.path.join(basedir, 'instances', 'index.html'), 'w') as idx:
        body = '<ul>'
        for atype, instances in instance_info.items():
            body += '<li><b>{0}</b><ul>'.format(atype)
            for iid in instances:
                body += '<li><a href="{{ROOT}}/instances/{0}.html">#{0}</a></li>'.format(iid)
            body += '</ul></li>'
        body += "</ul>"
        idx.write(MakePage(title='Instance Index'.format(atom.ID), depth=1, body=body))

    # Tiles
    with open(os.path.join(basedir, 'index.html'), 'w') as f:
        body = '<table class="prettytable"><thead><tr><th>Icon</th><th>ID</th><th>Instances</th></tr></thead><tbody>'
        for tile in dmm.tileTypes:
            body += '<tr><td><img src="tiles/{0}.png" height="96" width="96" /></td><th>{0}</th><td><ul>'.format(tile.ID)
            for atom in tile.SortAtoms():
                body += '<li><a href="{{ROOT}}/instances/{0}.html">#{0}</a> - {1}</li>'.format(atom.ID, atom.path)
            body += '</ul></td></tr>'
            img = tile.RenderToMapTile(0, os.path.dirname(sys.argv[1]), MapRenderFlags.RENDER_STARS)
            if img is None: continue
            pass_2 = tile.RenderToMapTile(1, os.path.dirname(sys.argv[1]), 0)
            if pass_2 is not None:
                img.paste(pass_2, (0, 0, 96, 96), pass_2)
            img.save(os.path.join(basedir, 'tiles', '{0}.png'.format(tile.ID)), 'PNG')
        body += '</tbody></table>'
        f.write(MakePage(title='Tile Index'.format(atom.ID), depth=0, body=body))
