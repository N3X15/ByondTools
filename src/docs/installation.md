# Installation

Getting started with BYONDTools is easy.  First, we need a recent version of Python.

## Installing Python

### Windows
If you're on Windows, go to [https://python.org]() and download their latest 3.9 installer.

Install with defaults.

### Windows with Chocolatey
This requires you to run cmd.exe with administrative privileges:

```{code} shell
choco install -y python
```

### Linux

You probably already have Python 3 installed.

## Installing BYONDTools

### Globally
To install BYONDTools to your system:

```{code} shell
# Requires administrative privileges (sudo on Linux)

# Upgrade pip
python3 -m pip install -U pip

# Install BYONDTools bleeding edge
pip3 install -U git+https://gitlab.com/N3X15/ByondTools.git
```

### Project Using `pipenv` (RECOMMENDED)

```{code} shell
# Upgrade pip
python3 -m pip install -U pip

# Upgrade/install pipenv
pip3 install --user --upgrade pipenv

# Install BYONDTools bleeding edge
pipenv install git+https://gitlab.com/N3X15/ByondTools.git
```

## Next Steps

Onwards, to the [API Reference](api/index.md)!

You can also look at our [examples](https://gitlab.com/N3X15/ByondTools/-/tree/master/examples) directory.