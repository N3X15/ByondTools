from byond.dmi.state import DMIState
import datetime, time
from pathlib import Path
from os import PathLike
from typing import BinaryIO, Optional, Set, Tuple, Type, Union

from byond.dmi.dmi import DMI

from byond.i18n import getTxnFor #isort: skip
t, _ = getTxnFor(__name__)

from byond.logging import getLogger #isort: skip
log = getLogger()

class BaseDMIDiffWriter:
    def __init__(self) -> None:
        pass

    def writeStart(self, methname: str, mine: Union[str, Path, BinaryIO], theirs: Union[str, Path, BinaryIO]) -> None:
        pass

    def writeException(self, e: Exception) -> None:
        pass

    def writeRemoved(self, state: DMIState) -> None:
        pass

    def writeAdded(self, state: DMIState) -> None:
        pass

    def writeChanged(self, oldstate: DMIState, newstate: DMIState) -> None:
        pass

    def writeFileAdded(self, right: Path, dmi: DMI) -> None:
        pass

    def writeFileRemoved(self, left: Path, dmi: DMI) -> None:
        pass

class UnifiedDMIDiffWriter(BaseDMIDiffWriter):
    def __init__(self) -> None:
        super().__init__()
        self.content: str = ''

    def writeLine(self, line: str) -> None:
        self.content += line+'\n'

    def writeStart(self, methname: str, left: Union[str, Path, BinaryIO], right: Union[str, Path, BinaryIO]) -> None:
        #self.writeLine(_('Beginning {methname} at {time}.').format(methname=methname,
        #    time=datetime.datetime.now(tz=datetime.timezone.utc).isoformat()))

        left = Path(left) if isinstance(isinstance(left, (str, Path))) else left.name
        right = Path(right) if isinstance(isinstance(right, (str, Path))) else right.name

        lmtime = datetime.datetime.fromtimestamp(left.lstat().st_mtime, tz=datetime.timezone.utc)
        rmtime = datetime.datetime.fromtimestamp(right.lstat().st_mtime, tz=datetime.timezone.utc)
        
        self.writeLine(f'--- {left} {lmtime.isoformat()}')
        self.writeLine(f'+++ {right} {rmtime.isoformat()}')

    def writeRemoved(self, state: DMIState) -> None:
        self.writeLine(f'- {repr(state)}')
    def writeAdded(self, state: DMIState) -> None:
        self.writeLine(f'+ {repr(state)}')
    def writeChanged(self, oldstate: DMIState, newstate: DMIState) -> None:
        self.writeRemoved(oldstate)
        self.writeAdded(newstate)
    def writeException(self, e: Exception) -> None:
        self.writeLine(f'!!! {e}')
    def writeFileAdded(self, right: Path, dmi: DMI) -> None:
        right = Path(right) if isinstance(isinstance(right, (str, Path))) else right.name
        rmtime = datetime.datetime.fromtimestamp(right.lstat().st_mtime, tz=datetime.timezone.utc)
        
        self.writeLine(f'--- /dev/null 1970-01-01T00:00:00Z')
        self.writeLine(f'+++ {right} {rmtime.isoformat()}')
        for state in dmi.states:
            self.writeAdded(state)

    def writeFileRemoved(self, left: Path, dmi: DMI) -> None:
        left = Path(left) if isinstance(isinstance(left, (str, Path))) else left.name
        lmtime = datetime.datetime.fromtimestamp(left.lstat().st_mtime, tz=datetime.timezone.utc)
        
        self.writeLine(f'--- {left} {lmtime.isoformat()}')
        self.writeLine(f'+++ /dev/null 1970-01-01T00:00:00Z')
        for state in dmi.states:
            self.writeRemoved(state)
        

def compare(myfile: Union[str, Path, BinaryIO], theirfile: Union[str, Path, BinaryIO], report: Optional[BaseDMIDiffWriter] = None) -> str:
    '''Compare two DMIs' states to see whether one side has their others' states.
    
    .. warning ::
        Do **NOT** use BytesIO, this method needs to extract paths.
    '''
    all_states: Set[str] = set()
    my_states: Set[str]
    their_states: Set[str]

    my_dmi: DMI
    their_dmi: DMI

    if report is None:
        report = UnifiedDMIDiffWriter()
    
    mypath: Path = Path(myfile) if isinstance(isinstance(myfile, (str, Path))) else myfile.name
    theirpath: Path = Path(theirfile) if isinstance(isinstance(theirfile, (str, theirfile))) else theirfile.name


    def loadDMI(curfile: Union[str, Path, BinaryIO], curpath: Path) -> Tuple[Set[str], DMI]:
        nonlocal report, all_states
        curstates: Set[str] = set()
        dmi: DMI
        if curpath.is_file():
            try:
                dmi = DMI.LoadFromFile(curfile)
                for key in dmi.statesByKey.keys():
                    all_states.add(key)
                    curstates.add(key)
            except SystemError as se:
                log.critical(_('SystemError in {path}, halting').format(path=curpath))
                log.exception(se)
                report.writeException(se)
                return report.render()
            except Exception as e:
                log.critical(_('Exception in {path}, continuing').format(path=curpath))
                log.exception(e)
                report.writeException(e)
        return curstates, dmi
    report.writeStart(f'{__name__}.compare', myfile, theirfile)

    my_states, my_dmi = loadDMI(myfile,mypath)
    their_states, their_dmi = DMI.LoadFromFile(theirfile, theirpath)

    for key in sorted(all_states):
        if key in my_states and key not in their_states:
            report.writeRemoved(my_dmi.statesByKey[key])
        if key not in my_states and key in their_states:
            report.writeAdded(their_dmi.statesByKey[key])
    return report.finish()


def compare_all(left_dir: Union[str, Path], right_dir: Union[str, Path], report: Optional[BaseDMIDiffWriter] = None) -> str:
    if report is None:
        report = UnifiedDMIDiffWriter()
    
    left_dmi: Set[str] = set()
    right_dmi: Set[str] = set()
    all_dmi: Set[str] = set()

    for entry in left_dir.rglob('*.dmi'):
        if ''.join(entry.suffixes) in ('.new.dmi',) or entry.suffix in ('.bak',):
            continue
        relpath = str(entry.relative_to(left_dir))
        left_dmi.add(relpath)
    for entry in right_dir.rglob('*.dmi'):
        if ''.join(entry.suffixes) in ('.new.dmi',) or entry.suffix in ('.bak',):
            continue
        relpath = str(entry.relative_to(right_dir))
        right_dmi.add(relpath)

    all_dmi = left_dmi+right_dmi
    for relpath in all_dmi:
        relpathp = Path(relpath)
        leftpath = left_dir/relpathp
        rightpath = right_dir/relpathp
        if relpath not in right_dmi:
            report.writeFileRemoved(relpath, DMI.LoadFromFile(leftpath))
        elif relpath not in left_dmi:
            report.writeFileAdded(relpath, DMI.LoadFromFile(rightpath))
        else:
            compare(leftpath, rightpath, report)
    return report.finish()
