
import sys, argparse, os, re
from byond import ObjectTree, Map, MapRenderFlags
from byond.basetypes import Atom, PropertyFlags
from byond.map.format.dmm import DMMFormat

def _register_transcribe(subp: argparse.ArgumentParser) -> None:
    _transcribe = subp.add_parser('transcribe', help='Re-write a map file (used for parser debugging).')
    _transcribe.add_argument('-O', '--output', dest='output', type=str, help='The other side.', metavar='map.trans.dmm', nargs='?')
    _transcribe.add_argument('subject', type=str, help='Map file to re-write.', metavar='map.dmm')
    _transcribe.set_defaults(cmd=cmd_transcribe)

def cmd_transcribe(args: argparse.ArgumentParser) -> None:
    if not os.path.isfile(args.map):
        print('File {0} does not exist.'.format(args.mine))
        sys.exit(1)
    if not os.path.isfile(args.project):
        print('DM Environment File {0} does not exist.'.format(args.project))
        sys.exit(1)

    dmm = Map(forgiving_atom_lookups=True)
    dmm.Load(args.map, format='dmm')

    #fmt = DMMFormat(dmm)

    nz = len(dmm.zLevels)
    for z in range(nz):
        basename,ext = os.path.splitext(args.map)
        outfile='{0}-{1}{2}'.format(basename,z+1,ext)
        print('>>> Splitting z={}/{} to {}'.format(z+1,nz,outfile))
        output = Map(forgiving_atom_lookups=True)
        currentZLevel = dmm.zLevels[z]
        newZLevel = output.CreateZLevel(currentZLevel.height, currentZLevel.width)
        #newZLevel.initial_load=True
        for y in range(currentZLevel.width):
            for x in range(currentZLevel.height):
                newTile = newZLevel.GetTile(x, y)
                for atom in currentZLevel.GetTile(x,y).GetAtoms():
                    newTile.AppendAtom(atom.copy(toNewMap=True))
        #newZLevel.initial_load=False
        output.Save(outfile, format='dmm')
