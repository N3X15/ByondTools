"""
BYOND Base Packet Class

Created with help from tobba.
"""
import enum, struct, logging
from typing import Any, Union
def DefinePacket():
    packetsByID = {}
    packetsByName = {}
    allPackets = []
    def handler(_cls):
        nonlocal packetsByID, packetsByName, allPackets
        allPackets += [_cls]
        packetsByID[_cls.ID] = _cls
        packetsByName[_cls.Name] = _cls
        return _cls
    handler.all = allPackets
    handler.byID = packetsByID
    handler.byName = packetsByName
    return handler

# Decorator
Packet = DefinePacket()

class ENetTypes(enum.IntEnum):
    BYTE = 0
    SHORT = 1
    LONG = 2
    STRING = 3

    min_lens = [1, 2, 4, None]

    @staticmethod
    def GetMinLength(t):
        o = ENetTypes.min_lens[t.value]
        if o:
            return o
        return 0

class BaseFieldInfo(object):
    TypeName = ''
    MinSize = 0
    def __init__(self, name: str, datatype: ENetTypes, opts: dict=None):
        if opts is None:
            opts = {}
        self.name: str = name
        self.type: ENetTypes = datatype
        self.options: dict = opts

    def getEncodedLength(self, msg: bytes) -> int:
        return 0

    def getValueLength(self, value: Any) -> int:
        return 0

    def deserialize(self, msg: bytes) -> Any:
        return b''

    def serialize(self, value: Any) -> bytes:
        return b''

class ByteField(BaseFieldInfo):
    TypeName = 'unsigned char'
    MinSize=1
    def __init__(self, name: str, opts: dict=None):
        super().__init__(name, ENetTypes.BYTE, opts)

    def getEncodedLength(self, msg: bytes) -> int:
        return 1

    def getValueLength(self, value: Any) -> int:
        return 1

    def deserialize(self, msg: bytes) -> Any:
        return struct.unpack('B', msg)  # Unsigned char

    def serialize(self, value: Any) -> bytes:
        return struct.pack('B', value)  # Unsigned char

class ShortField(BaseFieldInfo):
    TypeName = 'short'
    MinSize=2
    def __init__(self, name: str, opts: dict=None):
        super().__init__(name, ENetTypes.SHORT, opts)

    def getEncodedLength(self, msg: bytes) -> int:
        return 2

    def getValueLength(self, value: Any) -> int:
        return 2

    def deserialize(self, msg: bytes) -> Any:
        return struct.unpack('>H', msg) # short

    def serialize(self, value: Any) -> bytes:
        return struct.pack('>H', value)  # short

class LongField(BaseFieldInfo):
    TypeName = 'long'
    MinSize=4
    def __init__(self, name: str, opts: dict=None):
        super().__init__(name, ENetTypes.LONG, opts)

    def getEncodedLength(self, msg: bytes) -> int:
        return 4

    def getValueLength(self, value: Any) -> int:
        return 4

    def deserialize(self, msg: bytes) -> Any:
        return struct.unpack('>L', msg)

    def serialize(self, value: Any) -> bytes:
        return struct.pack('>L', value)

class StringField(BaseFieldInfo):
    TypeName = 'string'
    MinSize=1
    def __init__(self, name: str, opts: dict=None):
        super().__init__(name, ENetTypes.STRING, opts)
        self.encoding = opts.pop('encoding', 'ascii')

    def getEncodedLength(self, msg: bytes) -> int:
        return msg[:msg.index(b'\x00')+1]

    def getValueLength(self, value: Any) -> int:
        return len(value.encode(self.encoding))+1 # NUL byte

    def deserialize(self, msg: bytes) -> Any:
        # Strip the \x00
        return msg[:-1].decode(self.encoding)

    def serialize(self, value: Any) -> bytes:
        return value.encode(self.encoding)+b'\x00'

FIELD_DATA_TYPES = {
    ENetTypes.BYTE: ByteField,
    ENetTypes.SHORT: ShortField,
    ENetTypes.LONG: LongField,
    ENetTypes.STRING: StringField,
}

class BasePacket(object):
    ID = 0
    Name = ''
    def __init__(self):
        self.__field_data = []
        self.min_length = 0

        self.length = 0
        self.sequence = 0

    def addField(self, datatype: ENetTypes, propname: str, **kwargs):
        '''
        Associate a part of a packet to a field in this class
        '''
        field = FIELD_DATA_TYPES[datatype](propname, kwargs)
        self.__field_data += [field]
        self.min_length += field.MinSize

    def addByteField(self, propname: str, initial: bytes=b''):
        self.addField(ENetTypes.BYTE, propname)
        return initial

    def addShortField(self, propname: str, initial: int=0):
        self.addField(ENetTypes.SHORT, propname)
        return initial

    def addLongField(self, propname: str, initial: int=0):
        self.addField(ENetTypes.LONG, propname)
        return initial

    def addStringField(self, propname: str, encoding: str='ascii', initial: int=''):
        self.addField(ENetTypes.STRING, propname, encoding=encoding)
        return initial

    def decode(self, key: bytes, data: bytes) -> bytes:
        checksum = data[-1]
        data=data[:-1]
        a = 0
        for i in range(len(data)):
            data[i] -= key >> (a & 0x1f) | a
            a = (a | data[i]) & 0xff
        assert a == checksum
        return data

    def deserialize(self, msg):
        if len(msg) < self.min_length:
            logging.error('Received truncated packet {0}: min_length={1}, msg.len={2}'.format(self.Name, self.min_length, len(msg)))

        # TIME FOR ASSUMPTIONS!
        pos = 0
        for idx, field in enumerate(self.__field_data):
            fieldlength = field.getEncodedLength(msg[pos:])
            rawdata = msg[pos:pos+fieldlength]
            unpacked = field.deserialize(rawdata)
            pos += fieldlength
            propname = field.name

            setattr(self, propname, unpacked)

    def serialize(self):
        msg = b''
        for idx, field in enumerate(self.__field_data):
            value = getattr(self, field.name)
            msg += field.serialize(value)
        return msg
