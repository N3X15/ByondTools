# byond.dmi.frame

## Classes


### class byond.dmi.frame.DMIFrame()
Representation of a frame within a `byond.dmi.dmi.DMI`.

Frames keep track of their own index, their directional icons, and their delay.


#### delay(: int)
Delay in deciseconds


#### directions(: Dict[byond.directions.EDirection, Optional[PIL.Image.Image]])
Directional icons in the frame


#### index(: int)
Frame order in the state
