from byond.dmi.frame import DMIFrame
import math
from PIL import Image as Pillow
from PIL.Image import Image

from byond.dmi.state import DMIState
from byond.directions import EDirection
class DMIImageWriter:
    def __init__(self) -> None:
        self.image: Image
        self.max_x: int
        self.max_y: int

    def writeToImage(self, dmi: 'byond.dmi.dmi.DMI') -> Image:
        spritesNeeded = sum([s.getImageCount() for s in dmi.states])
        
        # Next bit borrowed from DMIDE.
        icons_per_row = math.ceil(math.sqrt(spritesNeeded))
        rows = icons_per_row

        if spritesNeeded > icons_per_row * rows:
            rows += 1

        self.image = Pillow.new('RGBA', (int(icons_per_row * dmi.icon_width), int(rows * dmi.icon_height)))
        
        state: DMIState
        d: EDirection
        i: int
        x: int = 0
        y: int = 0
        frame: DMIFrame
        for state in dmi.states:
            for d in state.directions:
                for i in range(state.frameCount):
                    self.image.paste(state.frames[i].directions[d], (x*dmi.icon_width, y*dmi.icon_height))
                    x += 1
                    if x >= icons_per_row:
                        x = 0
                        y += 1
        return self.image