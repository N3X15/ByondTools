
import sys, argparse, os, re
from byond import ObjectTree, Map, MapRenderFlags
from byond.basetypes import Atom, PropertyFlags
from byond.map.format.dmm import DMMFormat

def _register_diff(subp: argparse.ArgumentParser) -> None:
    _compare = subp.add_parser('diff', help='Compare two map files and generate a map patch.')
    _compare.add_argument('-O', '--output', dest='output', type=str, help='The other side.', metavar='mine.dmdiff', nargs='?')
    _compare.add_argument('theirs', type=str, help='One side of the difference', metavar='theirs.dmm')
    _compare.add_argument('mine', type=str, help='The other side.', metavar='mine.dmm')
    _compare.set_defaults(cmd=cmd_diff)

def cmd_diff(args: argparse.ArgumentParser) -> None:
    if not os.path.isfile(args.theirs):
        print('File {0} does not exist.'.format(args.theirs))
        sys.exit(1)
    if not os.path.isfile(args.mine):
        print('File {0} does not exist.'.format(args.mine))
        sys.exit(1)
    if not os.path.isfile(args.project):
        print('DM Environment File {0} does not exist.'.format(args.project))
        sys.exit(1)

    theirs_dmm = Map(forgiving_atom_lookups=True)
    theirs_dmm.Load(args.theirs, format='dmm')

    mine_dmm = Map(forgiving_atom_lookups=True)
    mine_dmm.Load(args.mine, format='dmm')

    ttitle, _ = os.path.splitext(os.path.basename(args.theirs))
    mtitle, _ = os.path.splitext(os.path.basename(args.mine))

    format = DMMFormat(theirs_dmm)

    output = '{} - {}.dmmpatch'.format(ttitle, mtitle)

    if args.output:
        output = args.output
    with open(output, 'w') as f:
        stats = {
            'diffs':0,
            'tilediffs':0,
            'tiles':0,
            'atoms':0
        }
        print('Comparing maps...')
        for z in range(len(theirs_dmm.zLevels)):
            t_zlev = theirs_dmm.zLevels[z]
            m_zlev = mine_dmm.zLevels[z]
            if t_zlev.height != m_zlev.height or t_zlev.width != m_zlev.width:
                print('!!! ZLEVEL {} HEIGHT/WIDTH MISMATCH: ({},{}) != ({},{})'.format(z, t_zlev.height, t_zlev.width, m_zlev.height, m_zlev.width))
                continue
            print(" Scanning z-level {} ({}x{})...".format(z, t_zlev.height, t_zlev.width))
            for y in range(t_zlev.height):
                for x in range(t_zlev.width):
                    CHANGES = {}

                    tTile = theirs_dmm.GetTileAt(x, y, z)
                    # if tTile is None:
                    #    print('!!! THEIRS <{},{},{}>: Tile object is None!'.format(x, y, z))
                    #    #return
                    mTile = mine_dmm.GetTileAt(x, y, z)
                    # if tTile is None:
                    #    print('!!! MINE <{},{},{}>: Tile object is None!'.format(x, y, z))
                    #    #return

                    theirs = {}
                    mine = {}
                    all_keys = set()

                    if tTile:
                        for A in tTile.GetAtoms():
                            key = format.SerializeAtom(A)
                            all_keys.add(key)
                            if key not in theirs:
                                theirs[key] = [A, 0]
                            theirs[key][1] += 1
                    if mTile:
                        for A in mTile.GetAtoms():
                            key = format.SerializeAtom(A)
                            all_keys.add(key)
                            if key not in mine:
                                mine[key] = [A, 0]
                            mine[key][1] += 1

                    removals = set()
                    additions = set()
                    kept = {}

                    for key in all_keys:
                        change = None
                        minecount = 0
                        if key in mine:
                            minecount = mine[key][1]
                        theircount = 0
                        if key in theirs:
                            theircount = theirs[key][1]
                        delta = minecount - theircount
                        if delta < 0:
                            change = '-'
                            removals.add(key)
                        elif delta > 0:
                            change = '+'
                            additions.add(key)
                        if minecount > 0 and delta <= 0:
                            kept[key] = minecount
                        if change is not None:
                            CHANGES[key] = [change, abs(delta), minecount, theircount]
                        stats['tiles'] += 1

                    def writeChanges(f, source, CHANGES):
                        for key in source:
                            change, amount, mc, tc = CHANGES[key]
                            # change, amount, _, _ = changedat
                            # f.write(' # {} vs {}\n'.format(mc, tc))
                            abs_amt = abs(amount)
                            if abs_amt > 1:
                                f.write(' {}{} {}\n'.format(change, abs_amt if mc > 0 else '*', key))
                            else:
                                f.write(' {} {}\n'.format(change, key))
                            stats['diffs'] += abs_amt

                    if len(CHANGES) > 0:
                        f.write('<{},{},{}>\n'.format(x, y, z))
                        stats['tilediffs'] += 1
                        f.write(' @CHECK {before} {after} {tiledat}\n'.format(before=tTile.GetHash(), after=mTile.GetHash(), tiledat=format.SerializeTile(mTile)))
                        if len(kept) == 0:
                            f.write(' -ALL\n')
                        else:
                            writeChanges(f, removals, CHANGES)
                        """
                        for key,count in kept.items():
                            if count > 1:
                                f.write(' ={} {}\n'.format(count, key))
                            else:
                                f.write(' = {}\n'.format(key))
                        """
                        writeChanges(f, additions, CHANGES)
        print('Compared maps: {} differences in {} tiles.'.format(stats['diffs'], stats['tilediffs']))
        print('Total: {} atoms, {} tiles.'.format(stats['diffs'], stats['tilediffs']))
