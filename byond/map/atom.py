from byond.typepath import TypePath
from typing import TYPE_CHECKING, Optional
from byond.point3 import Point3
from collections import OrderedDict
if TYPE_CHECKING:
    from byond.map.map import Map
    from byond.map.level import MapLevel
UNDEFINED = TypePath('/undefined')
class Atom:
    def __init__(self, level: Optional['MapLevel'] = None, loc: Optional[Point3] = None) -> None:
        self.map: 'Map' = level.map
        self.level: 'MapLevel' = level

        self.loc: Point3 = loc

        self.type: TypePath = UNDEFINED
        self.attributes: OrderedDict = OrderedDict({})

    def __str__(self) -> str:
        return '{'+','.join((f'{k}={v}' for k, v in self.attributes.items()))+'}'
