from byond.map.map import Map
from byond.map.mapreader import MapReader
from byond.map.dmm.parser import Lark_StandAlone as DMMParser, Transformer, Tree

class DMMTransformer(Transformer):
    def float(self, content):
        return float(content)
        
class DMMReader(MapReader):
    def __init__(self) -> None:
        super().__init__()
        self.parser = DMMParser()

    def load(self, data: str) -> Map:
        # Run map code through the LALR(1) parser
        tree: Tree = self.parser.parse(data)
        print(tree.pretty())
        