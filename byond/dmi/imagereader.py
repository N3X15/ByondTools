from point2d.point2d import Point2D
from byond.dmi.frame import DMIFrame
from byond.dmi.state import DMIState, EStateDirections
from byond.dmi.exceptions import DMIMissingDescriptionError
from pathlib import Path
from typing import BinaryIO, Union
from PIL import Image as Pillow
from PIL.Image import Image

from byond.logging import getLogger #isort: skip
log = getLogger(__name__)

from byond.i18n import getTxnFor #isort: skip
t,_=getTxnFor(__name__)

class DMIImageReader:
    '''
    This handles *JUST* the image portion of things.  
    
    Must be instantiated with the preloaded metadata found in the DMI.
    '''
    def __init__(self, dmi: 'byond.dmi.dmi.DMI') -> None:
        self.image: Image = None
        self.dmi: 'byond.dmi.dmi.DMI' = dmi
    
    def loadFromFile(self, filename: Union[str, Path]) -> None:
        self.image = Pillow.open(filename)
        self._internalLoad()
    def loadFromBytes(self, filename: bytes) -> None:
        self.image = Pillow.open(filename)
        self._internalLoad()
    def loadFromHandle(self, handle: BinaryIO) -> None:
        self.image = Pillow.open(handle)
        self._internalLoad()
    def loadFromImage(self, img: Image) -> None:
        self.image = img
        self._internalLoad()

    def _internalLoad(self) -> None:
        image = self.image
        # This is a stupid hack to work around BYOND generating indexed PNGs with unspecified transparency.
        # Uncorrected, this will result in PIL(low) trying to read the colors as alpha.
        if image.mode == 'P':
            # If there's no transparency, set it to black.
            if 'transparency' not in image.info:
                log.warning(_('Indexed PNG does not specify transparency! Setting black as transparency. image.info = {1}').format(self.filename, repr(image.info)))
                self.image.info['transparency'] = 0

        # Always use RGBA, it causes fewer problems.
        if self.image.mode != 'RGBA':
            self.image = self.image.convert('RGBA')

        width, height = image.size

        # Load pixels from image
        pixels = image.load()
        
        state = None
        x = 0
        y = 0
        max_x = width / self.dmi.icon_width
        max_y = height / self.dmi.icon_height
        statelist = []
        
        state: DMIState
        # We've already pre-alloc'd frames when reading metadata.
        for state in self.dmi.states:
            for direction in state.directions:
                for i in range(state.frameCount):
                    state.frames[i].directions[direction] = self.extractSpriteFromImageAt(image, height, width, x, y)
                    x += 1
                    if x >= max_x:
                        x = 0
                        y += 1

        self.dmi._image_loaded=True
    
    def extractSpriteFromImageAt(self, image: Image, height: int, width: int, x: int, y: int) -> Image:
        # Remember how the old DMI had a gigantic fucking function to do this?
        # Check this shit out:
        ni = image.crop((x*width, y*height, (x+1)*width, (y+1)*height))
        assert ni.size[0] == width
        assert ni.size[1] == height
        return ni