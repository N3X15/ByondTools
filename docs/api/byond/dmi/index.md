# BYOND DMI API

BYOND stores images in an atlased PNG with embedded metadata called a DMI (DreamMaker Icon).

This API lets you mess with DMI using the v4.0 metadata format. More formats will become available over time.

## Important Classes


* `byond.dmi.dmi.DMI`


* `byond.dmi.frame.DMIFrame`


* `byond.dmi.state.DMIState`

## Important Enums


* `byond.dmi.state.EStateDirections`

## All Sub-Modules


* `byond.dmi._metaentry` (You shouldn’t mess with this!)


* `byond.dmi.dmi`


* `byond.dmi.exceptions`


* `byond.dmi.frame`


* `byond.dmi.imagereader`


* `byond.dmi.imagewriter`


* `byond.dmi.metareader`


* `byond.dmi.metawriter`


* `byond.dmi.state`
