from .packet import BasePacket, Packet

@Packet
class TopicPacket(BasePacket):
    ID = 129
    Name = 'Topic'
    def __init__(self):
        super().__init__()
        self.payload: str = self.addStringField('payload', encoding='ascii')
