from byond.point3 import Point3
from byond.map.atom import Atom
from typing import TYPE_CHECKING, Optional
from byond.map.level import MapLevel

if TYPE_CHECKING:
    from byond.map import MapLevel
    
class MapTurf(Atom):
    def __init__(self, level: Optional[MapLevel] = None, loc: Optional[Point3] = None) -> None:
        super().__init__(level=level, loc=loc)
