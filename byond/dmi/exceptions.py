from typing import Any
from byond.i18n import getTxnFor #isort: skip
t,_=getTxnFor(__name__)

class _BaseDMIError(Exception):
    MESSAGE = ''
    def __init__(self) -> None:
        super().__init__(self.MESSAGE)

class DMIMissingDescriptionError(_BaseDMIError):
    '''
    thrown when a PNG is missing its Description metadata.
    '''
    MESSAGE = _('DMI PNG metadata is missing a Description field')

class DMIInvalidFormatError(_BaseDMIError):
    '''
    Thrown when a given file is not a PNG.
    '''
    MESSAGE = _('DMI is not of the PNG format')

class DMIEmptyDescriptionError(_BaseDMIError):
    '''
    Thrown when the PNG description field is an empty string.
    '''
    MESSAGE = _('DMI has an empty Description field.')

class DMIUnhandledTypeError(_BaseDMIError):
    '''
    Thrown when attempting to serialize a type that isn't handled by set().serialize().
    '''
    MESSAGE = _('Unable to serialize type {type}.')
    def __init__(self, typ: str) -> None:
        self.type: Any = typ
        super().__init__(self.MESSAGE.format(type=typ))

class DMIMissingPropertyError(_BaseDMIError):
    '''
    Thrown from when a required property is missing from an entry.
    '''
    MESSAGE = _('DMI metadata entry {name} of type {type} is missing required property {key}')
    def __init__(self, name: str, typ: Any, key: str) -> None:
        self.name: str = name
        self.type: Any = typ
        self.key: str = key
        super().__init__(self.MESSAGE.format(name=repr(name), type=typ, key=repr(key)))

class DMIUnrecognizedVersionError(_BaseDMIError):
    '''
    Thrown when the DMI metadata version is not recognized by the reader.
    '''
    MESSAGE = _('DMI header specifies an unhandled version: {version}')
    def __init__(self, version: str) -> None:
        self.version: str = version
        super().__init__(self.MESSAGE.format(version=repr(version)))