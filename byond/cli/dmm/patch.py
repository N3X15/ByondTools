
import sys, argparse, os, re
from byond import ObjectTree, Map, MapRenderFlags
from byond.basetypes import Atom, PropertyFlags
from byond.map.format.dmm import DMMFormat

def _register_patch(subp: argparse.ArgumentParser) -> None:
    _patch = subp.add_parser('patch', help='Apply a map patch.')
    _patch.add_argument('-O', '--output', dest='output', type=str, help='Where to place the patched map. (Default is to overwrite input map)', metavar='mine.dmdiff', nargs='?')
    _patch.add_argument('--clobber', dest='clobber', action="store_true", help='Overwrite conflicts')
    _patch.add_argument('--changemarker', dest='changemarker', type=str, default='/obj/effect/byondtools/changed', help='Overlay object used to mark a turf as changed.')
    _patch.add_argument('patches', action='append', nargs='+', help='Patch(es) to apply.', metavar='patch.dmmpatch')
    _patch.add_argument('map', type=str, help='The map to change.', metavar='map.dmm')
    _patch.set_defaults(cmd=cmd_patch)

def cmd_patch(args: argparse.ArgumentParser) -> None:
    print(repr(args.patches))
    for i in range(len(args.patches[0])):
        patch=args.patches[0][i]
        if not os.path.isfile(patch):
            print('File {0} does not exist.'.format(patch))
            sys.exit(1)
    if not os.path.isfile(args.map):
        print('File {0} does not exist.'.format(args.mine))
        sys.exit(1)
    if not os.path.isfile(args.project):
        print('DM Environment File {0} does not exist.'.format(args.project))
        sys.exit(1)

    dmm = Map(forgiving_atom_lookups=True)
    dmm.Load(args.map, format='dmm')

    fmt = DMMFormat(dmm)

    context = None
    added = 0
    removed = 0

    currentpatch = ''

    def changeMarker(ln):
        a = Atom(args.changemarker)
        a.setProperty('tag','{}:{}'.format(currentpatch,ln),flags=PropertyFlags.MAP_SPECIFIED)
        return a

    def printReport(context, added, removed):
        if context is not None:
            x, y, z = context
            print(' Z={} +{} -{}'.format(z, added, removed))

    REG_INSTRUCTION = re.compile(r'^(?P<change>[\+\-])(?P<amount>[0-9\*]+)?\s+(?P<atom>/.*)')

    for i in range(len(args.patches[0])):
        patch=args.patches[0][i]
        print('* Applying {}...'.format(patch))
        currentpatch=patch
        with open(patch) as f:
            ln = 0
            lz = -1
            skip_block = False
            for line in f:
                ln += 1
                line = line.strip()
                if line == '': continue
                if line.startswith('#'): continue
                if line.startswith('<') and line.endswith('>'):
                    strcoords = line.strip('<>').split(',')
                    newcoords = []
                    for coord in strcoords:
                        newcoords += [int(coord)]
                    if context is not None and lz != context[2]:
                        printReport(context, added, removed)
                        lz = context[2]
                        added = removed = 0
                    context = newcoords
                    skip_block = False
                    continue
                if line.startswith('@') and not skip_block:
                    # @CHECK before-hash after-hash atoms-block

                    x, y, z = context

                    if line.startswith('@CHECK'):
                        _, beforehash, afterhash, serdata = line.split(' ', 3)

                        if args.clobber:
                            #print('WARNING: <{},{},{}> has changed and will be overwritten. (--clobber)'.format(x, y, z))
                            t=fmt.consumeTileChunk(serdata, cache=False)
                            t.AppendAtom(changeMarker(ln))
                            dmm.SetTileAt(x, y, z, t)
                            skip_block = True
                            continue
                        curhash = dmm.GetTileAt(x, y, z).GetHash()
                        if afterhash == curhash:
                            print('Skipping <{},{},{}> (already what we expected)'.format(x, y, z))
                            skip_block = True
                            continue
                        #else: print('PRE {} != {}: OK'.format(curhash,afterhash))
                        if beforehash != curhash:
                            print('WARNING: <{},{},{}> has changed.  Operations on this tile may not be accurate!'.format(x, y, z))
                            continue
                        #else: print('OLD {} == {}: OK'.format(curhash,beforehash))

                if not skip_block and (line.startswith('+') or line.startswith('-')):
                    if line == '-ALL':
                        dmm.SetTileAt(x, y, z, dmm.CreateTile())
                        continue
                    m = REG_INSTRUCTION.match(line)
                    if m is None:
                        print('{}:{}: MALFORMED INSTRUCTION: {}'.format(args.patch, ln, line))
                        sys.exit(1)
                    amount = m.group('amount')
                    if amount == '*':
                        amount = 9999
                    else:
                        amount = int(m.group('amount') or 1)
                    change = m.group('change')

                    atom = fmt.consumeAtom(m.group('atom'), ln)
                    atom.filename = args.patch
                    atom.line = ln
                    if atom is None:
                        print('{}:{}: WARNING: Unable to parse instance specified by chunk {}'.format(args.patch, ln, m.group('atom')))
                        continue

                    x, y, z = context
                    if x == 0 and y == 0 and z == 0:
                        print('WE AT <0,0,0> SOMEFIN WRONG')

                    if z >= len(dmm.zLevels): continue

                    tile = dmm.GetTileAt(x, y, z)
                    if change == '-':
                        for _ in range(amount):
                            if tile.CountAtom(atom) > 0:
                                tile.RemoveAtom(atom, hash=False)
                                removed += 1
                            else: break
                    elif change == '+':
                        for _ in range(amount):
                            tile.AppendAtom(atom, hash=False)
                        added += amount
                    tile.UpdateHash()
                    # dmm.SetTileAt(x, y, z, tile)
                    # print('{} - {}'.format((x,y,z),tile))
            printReport(context, added, removed)

    print('Saving...')
    dmm.Save(args.output if args.output else args.map)
