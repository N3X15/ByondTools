# byond.dmi.state

```{eval-rst}
.. module:: byond.dmi.state
```

## Enums

```{eval-rst}
.. autoclass:: byond.dmi.state.EStateDirections
    :show-inheritance:
    :members:
```
## Classes

```{eval-rst}
.. autoclass:: byond.dmi.state.DMIState
    :members:
```