# byond.dmi.state

## Enums


### class byond.dmi.state.EStateDirections(value)
Bases: `enum.IntEnum`

Number of possible directions the state supports.


#### ALL( = 8)
NSEW + diagonals


#### CARDINAL( = 4)
NSEW


#### ONE( = 1)
South only

## Classes


### class byond.dmi.state.DMIState(name: Optional[str] = None, dir_count: Optional[byond.dmi.state.EStateDirections] = None, frame_count: Optional[int] = None, movement: Optional[bool] = None, alloc: bool = True)
A state is a collection of related animations with directionality and other information.

For instance, your DMI may be for an NPC that walks around, stands still, and talks to the player.

You would then have a state for walking, another for standing, and another for talking.  Each
state would contain animations for each direction.

**NOTE**: There are special movement states. For these, use the same name but set movement=True.


#### allocFrames(size: Optional[Tuple[int, int]] = None)
Allocate frames and, if size is not None, PIL icons of the given size.


* **Parameters**

    **size** – A 2-tuple containing (width, height) in pixels



#### directionCount(: byond.dmi.state.EStateDirections)
Number of directions (1,4,8)


#### property directions(: List[byond.directions.EDirection])
Get all available directions for this state.


#### frameCount(: int)
How many frames are needed


#### frames(: List[byond.dmi.frame.DMIFrame])
Data about each frame.


#### getImagesByDirection(direction: byond.directions.EDirection)
A list of all frames as PIL images, given direction.


#### hotspot(: Optional[point2d.point2d.Point2D])
Hotspot location


#### property loop(: bool)
Loops after playing?


#### property movement(: bool)
Movement state?


#### name(: str)
Name of the state.


#### property rewind(: bool)
Rewind after playing?


#### setImagesByDirection(direction: byond.directions.EDirection, images: List[Optional[PIL.Image.Image]])
Set list of all frames as PIL images, given direction.
