# BYONDTools

A set of tools for BYOND-based games.

## Overview
This is a toolkit for developers of [BYOND](https://byond.com)-based games
intended to do common, complex tasks that the engine itself does not currently
possess.  It also exposes common interfaces for developers wishing to obtain
information from the BYOND object tree, as well as maps, and DMIs.

<div style="margin-bottom: 27px;background-color: #fff;border: 1px solid transparent;border-radius: 4px;box-shadow: 0 1px 1px rgba(0,0,0,.05);border-color: #faebcc;">
<p style="color: #8a6d3b;background-color: #fcf8e3;border-color: #faebcc;padding: 15px;box-sizing: border-box;"><strong>Warning</strong></p>
<div>

This project is currently receiving a complete overhaul so it works on Python
3.8.

In addition, much of the code was written in 2013, when I still had a tenuous
grasp (at best) of how Python worked. I am working on cleaning this up.

Please be patient.

-- N3X

</div>
</div>


## Requirements
* Python >= 3.6
* Pillow
* numpy
* pyparsing

## Installing

### Windows

1. Install Python 3.x from https://python.org
1. Run `pip install byondtools`

### Linux

#### Ubuntu/Debian

1. `apt install python3.9`
1. `pip install byondtools`

## Use
Initial documentation was taking shape at http://ss13.pomf.se/wiki/index.php/User:N3X15/Guide_to_BYONDTools

I plan on moving that to docs/ soon.

## API Status

<table>
<thead>
<tr>
<th>API Component</th>
<th>State</th>
</tr>
</thead>
<tbody>
<tr>
<th>[DMI](doc/api/dmi.md)</th>
<td>WORKING! (Rewritten)</td>
</tr>
<tr>
<th>Maps</th>
<td>Pending Evaluation</td>
</tr>
<tr>
<th>Object Tree</th>
<td>Pending Evaluation</td>
</tr>
<tr>
<th>Scripting</th>
<td>Pending Evaluation</td>
</tr>
</tbody>
</table>

## Support
No commercial or official support is provided, but you are free to submit bug
reports or harass N3X15:

 * **Discord:** N3X15#7475 
 * **IRC:** #vgstation @ irc.rizon.net
 * **Email:** `python -c "print(bytes.fromhex('6e65786973656e7465727461696e6d656e7440676d61696c2e636f6d').decode('ascii'))"`
