import json
from enum import IntEnum, IntFlag
from pathlib import Path
from typing import Any, Dict, FrozenSet, List, Optional, OrderedDict, Tuple, Union

from PIL import Image as Pillow

from byond.directions import EDirection
from byond.dmi._metaentry import DMIMetaEntry, EDMIMetaEntryType
from byond.dmi.frame import DMIFrame
from point2d import Point2D

from byond.logging import getLogger #isort: skip
log = getLogger(__name__)

from byond.i18n import getTxnFor #isort: skip
t, _ = getTxnFor(__name__)

__ALL__ = ['EStateDirections', 'DMIState']


class EStateDirections(IntEnum):
    '''
    Number of possible directions the state supports.
    '''
    #: South only
    ONE      = 1
    #: NSEW
    CARDINAL = 4
    #: NSEW + diagonals
    ALL      = 8

class EStateFlags(IntFlag):
    '''
    Internal BYONDTools flags
    '''
    
    NONE = 0

    '''Is this a movement state?
    
    .. note::
    Exposed as :py:attr:`.movement`.
    '''
    MOVEMENT = 1

    '''Does this state loop?
    
    .. note::
    Exposed as :py:attr:`.loop`.
    '''
    LOOP     = 2

    '''Does this state rewind?
    
    .. note::
    Exposed as :py:attr:`.rewind`
    '''
    REWIND   = 4

def hotspotToString(hotspot: Optional[Point2D]) -> Optional[str]:
    if hotspot is None:
        return None
    return f'{round(hotspot.x)},{round(hotspot.y)}'
def stringToHotspot(hotspot: Optional[str]) -> Optional[Point2D]:
    if hotspot is None:
        return None
    x,y = hotspot.split(',')
    return Point2D(x, y)

def delaysToString(delays: List[int]) -> str:
    return ','.join([str(x) for x in delays])
def stringToDelays(delays: Optional[str]) -> List[int]:
    return [int(x) for x in delays.split(',')]

class DMIState:
    '''
    A state is a collection of related animations with directionality and other information.

    For instance, your DMI may be for an NPC that walks around, stands still, and talks to the player.

    You would then have a state for walking, another for standing, and another for talking.  Each
    state would contain animations for each direction.

    .. note:: 
        There are special movement states. For these, use the same name but set movement=True.
    '''

    '''List of valid keys.

    :meta private:
    '''
    VALID_KEYS: FrozenSet[str] = frozenset(['dirs', 'frames', 'hotspot', 'delay', 'movement', 'loop', 'rewind'])

    def __init__(self, name: Optional[str] = None, dir_count: Optional[EStateDirections] = None, frame_count: Optional[int] = None, movement: Optional[bool] = None, alloc: bool=True) -> None:
        #: Name of the state.
        self.name: str = name or ''
        
        #: Number of directions (1,4,8)
        self.directionCount: EStateDirections = dir_count or EStateDirections.ONE
        
        #: Hotspot location
        self.hotspot: Optional[Point2D] = None

        #: How many frames are needed
        self.frameCount: int = frame_count or 0

        #: Data about each frame.
        self.frames: List[DMIFrame] = []

        self.flags: EStateFlags = EStateFlags.NONE

        #: Movement state?
        self.movement: bool = movement or False

        #: Loops after playing?
        self.loop: bool = False

        #: Rewind after playing?
        self.rewind: bool = False

        if self.name != '' and alloc:
            self.allocFrames()

    def _get_delays(self) -> List[int]:
        return [x.delay for x in self.frames]
    def _set_delays(self, delays: List[int]) -> None:
        assert len(delays) == len(self.frames)
        for i in range(len(delays)):
            self.frames[i].delay = delays[i]
    delays = property(_get_delays, _set_delays)

    @property
    def directions(self) -> List[EDirection]:
        '''Get all available directions for this state.'''
        if self.directionCount == EStateDirections.ONE:
            return [EDirection.SOUTH]
        elif self.directionCount == EStateDirections.CARDINAL:
            return [EDirection.SOUTH, EDirection.NORTH, EDirection.WEST, EDirection.EAST]
        elif self.directionCount == EStateDirections.ALL:
            return [EDirection.SOUTH, EDirection.NORTH, EDirection.WEST, EDirection.EAST, EDirection.SOUTHEAST, EDirection.SOUTHWEST, EDirection.NORTHEAST, EDirection.NORTHWEST]

    
    @staticmethod
    def MakeKey(name: str, movement: bool = False) -> str:
        tags: str = ''
        if movement:
            tags += DMIState.MOVEMENT_TAG
        return '\t'.join([name, tags])

    @property
    def key(self) -> str:
        return self.MakeKey(self.name, self.movement)

    def _get_movement(self) -> bool:
        return self.flags & EStateFlags.MOVEMENT
    def _set_movement(self, on: bool):
        if on: 
            self.flags |= EStateFlags.MOVEMENT
        else:
            self.flags &= ~EStateFlags.MOVEMENT
    '''Is movement enabled?'''
    movement = property(_get_movement, _set_movement)
    
    def _get_loop(self) -> bool:
        return self.flags & EStateFlags.LOOP
    def _set_loop(self, on: bool):
        if on: 
            self.flags |= EStateFlags.LOOP
        else:
            self.flags &= ~EStateFlags.LOOP
    '''Is looping enabled?'''
    loop = property(_get_loop, _set_loop)
    
    def _get_rewind(self) -> bool:
        return self.flags & EStateFlags.LOOP
    def _set_rewind(self, on: bool):
        if on: 
            self.flags |= EStateFlags.LOOP
        else:
            self.flags &= ~EStateFlags.LOOP
    '''Is rewinding enabled?'''
    rewind = property(_get_rewind, _set_rewind)

    def allocFrames(self, size: Optional[Tuple[int, int]] = None) -> None:
        '''Allocate frames and, if `size` is not None, PIL icons of the given `size`.
        
        :param size:
            A 2-tuple containing (width, height) in pixels
        '''
        self.frames=[]
        frame: DMIFrame
        for i in range(self.frameCount):
            frame = DMIFrame()
            frame.index = i
            frame.directions = {d: (None if size is None else Pillow.new('RGBA', size)) for d in self.directions}
            self.frames.append(frame)

    def asDMIEntry(self) -> DMIMetaEntry:
        e = DMIMetaEntry(EDMIMetaEntryType.STATE, self.name)
        e.set('dirs', self.directionCount.value)
        e.set('hotspot', hotspotToString(self.hotspot), default=None)
        e.set('frames', self.frameCount)
        e.set('movement', self.movement, default=False)
        e.set('loop', self.loop, default=False)
        e.set('rewind', self.rewind, default=False)
        e.set('delay', self.delays)
        return e

    def fromDMIEntry(self, e: DMIMetaEntry) -> None:
        assert e.type == EDMIMetaEntryType.STATE
        self.name = e.name

        actual_keys = set(e.values.keys())
        for key in actual_keys:
            if key not in self.VALID_KEYS:
                log.warning(_('Unexpected DMI metadata key {key!r} in state {name!r}. This key-value pair will be ignored. Expected keys: {VALID_KEYS!r}').format(key=key,name=self.name,VALID_KEYS=list(self.VALID_KEYS)))

        self.directionCount = EStateDirections(e.get('dirs', '1', typ=int))
        self.frameCount = e.get('frames', '1', typ=int)

        self.allocFrames()

        self.hotspot = e.get('hotspot', None, typ=stringToHotspot)
        self.delays = e.get('delay', None, typ=int, from_list=True)
        self.movement = e.get('movement', False, typ=bool)
        self.loop = e.get('loop', False, typ=bool)
        self.rewind = e.get('rewind', False, typ=bool)

    def getImagesByDirection(self, direction: EDirection) -> List[Optional[Pillow.Image]]:
        '''A list of all frames as PIL images, given direction.'''
        assert direction in self.directions
        return [f.directions[direction] for f in self.frames]

    def setImagesByDirection(self, direction: EDirection, images: List[Optional[Pillow.Image]]) -> None:
        '''Set list of all frames as PIL images, given direction.'''
        assert direction in self.directions
        for i, f in enumerate(self.frames):
            f.directions[direction] = images[i]

    def getImageCount(self) -> int:
        return self.directionCount.value * self.frameCount

    def getSafeName(self) -> str:
        pass

    def serialize(self) -> Dict[str, Any]:
        return OrderedDict({
            'name': self.name,
            'directionCount': self.directionCount.value,
            'frameCount': self.frameCount,
            'flags': [x.name for x in self.flags],
            'hotspot': [self.hotspot.x, self.hotspot.y] if self.hotspot else None,
            'frames': [f.serialize() for f in self.frames]
        })

    def deserialize(self, data: Dict[str, Any]) -> None:
        self.name = data['name']
        self.directionCount = EStateDirections(data['directionCount'])
        self.frameCount = data['frameCount']
        self.flags = EStateFlags.NONE
        if 'flags' in data:
            flags = data.get('flags', 0)
            if isinstance(flags, int):
                flags |= EStateFlags(flags)
            elif isinstance(flags, list):
                for x in flags:
                    self.flags |= EStateFlags[x] if isinstance(x, str) else EStateFlags(x)
        self.frames = []
        for f in data.get('frames', []):
            frame = DMIFrame()
            frame.deserialize(f)

    def extractTo(self, toDir: Union[str, Path]) -> None:
        toDir = Path(toDir)
        toDir.mkdir(parents=True, exist_ok=True)
        (toDir / 'meta.json').write_text(json.dumps(self.serialize(), indent=2))
        for direction in self.directions:
            for i, frame in enumerate(self.frames):
                frame.directions[direction].save(toDir / direction.name.lower() / f'{i}.png', 'PNG')
