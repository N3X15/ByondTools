from .packet import BasePacket, Packet

@Packet
class VersionPacket(BasePacket):
    ID = 1
    Name = 'Version'
    def __init__(self):
        super().__init__()
        self.version: int  = self.addLongField('version')
        self.network: int  = self.addLongField('network')
        self.key: int      = self.addLongField('key')
        self.sequence: int = self.addShortField('sequence')
