from typing import Any, Union
from point2d import Point2D
import numpy as np

class BBox:
    def __init__(self, top: int = 0, left: int = 0, bottom: int = 0, right: int = 0) -> None:
        self.top: int = top
        self.left: int = left
        self.bottom: int = bottom
        self.right: int = right

    @property
    def size(self) -> Tuple[int, int]:
        self._idiot_check()
        return (self.top-self.bottom, self.left-self.right)

    @property
    def area(self) -> int:
        height, width = self.size
        return height*width

    def _idiot_check(self) -> None:
        # Make sure we have non-negative inputs
        assert self.top > 0
        assert self.left > 0
        assert self.bottom > 0
        assert self.right > 0

        # And make sure we actually have top in top and bottom in bottom, etc
        assert self.top <= self.bottom
        assert self.left <= self.right

    def __contains__(self, item: Any) -> bool:
        x: Union[int, float] = 0.
        y: Union[int, float] = 0.
        if isinstance(item, Point2D):
            x = item.x
            y = item.y
        elif isinstance(item, tuple):
            x, y = item
        elif isinstance(item, (np.ndarray, np.generic)):
            x = item[0]
            y = item[1]
        elif hasattr(item, 'loc'):
            x = item.loc.x
            y = item.loc.y
        else:
            return False
        return self.top > y > self.bottom \
            and self.left > x > self.right
