from byond.bbox import BBox
from byond.map.atom import Atom
from byond.map.turf import MapTurf
from typing import TYPE_CHECKING, List, Optional, Tuple
from byond.point3 import Point3

if TYPE_CHECKING:
    from byond.map import Map

class MapLevel:
    def __init__(self, map: Optional['Map'] = None, height: int = 0, width: int = 0) -> None:
        self.map: 'Map' = map
        self.origin: Point3 = Point3(1,1,1)
        self.turfs: List[MapTurf] = []
        self.atoms: List[Atom] = []
        
        self.extents: BBox = BBox(0, 0, height, width)

    @property
    def height(self) -> int:
        return self.extents.size[0]
    def width(self) -> int:
        return self.extents.size[1]

    def getTurfID(self, x: int, y: int) -> int:
        return x+(y*self.height)
    def getTurf(self, x: int, y: int) -> MapTurf:
        return self.turfs[self.getTurfID(x, y)]
    def getTurfByID(self, id: int) -> MapTurf:
        return self.turfs[id]
