import string
from buildtools import log, os_utils
def print_packet(packet: bytes) -> None:
    #log.info(" ".join("{:02x}".format(ord(c)) for c in packet))
    strbuf = ''
    hexbuf = []
    printable = string.digits + string.ascii_letters + string.punctuation
    printable = bytes(printable, 'ascii')
    log.info('Length: %dB (%s)', len(packet), os_utils.sizeof_fmt(len(packet)))
    log.info('Bytes: ' + repr(packet))

    def print_line(start):
        nonlocal hexbuf, strbuf
        log.info(f'{start:04X}   %s   %s',
                 (' '.join(hexbuf)).ljust(47), strbuf)
        hexbuf = []
        strbuf = ''
    for addr in range(len(packet)):
        c = packet[addr:addr + 1]
        #print(addr, c)
        if (addr > 0 and addr % 16 == 0):
            print_line(addr - len(strbuf))
        hexbuf += ['{:02X}'.format(ord(c))]
        strbuf += c.decode('ascii') if c in printable else '.'
    if len(strbuf) > 0:
        print_line(len(packet) - len(strbuf))
