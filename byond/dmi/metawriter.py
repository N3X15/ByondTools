from pathlib import Path
from typing import BinaryIO, Optional, Union

import PIL

from byond.dmi._metaentry import DMIMetaEntry
from PIL import PngImagePlugin
from PIL.Image import Image


class DMIMetaWriter:
    def __init__(self) -> None:
        pass

    def dump(self, dmi: 'byond.dmi.dmi.DMI') -> str:
        o = '#BEGIN DMI\n'
        e: DMIMetaEntry
        for e in [dmi.asDMIEntry()]+[s.asDMIEntry() for s in dmi.states]:
            o += e.dump()
        o += '\n#END DMI'
        return o

    @classmethod
    def GenMetaFromDMI(cls, dmi: 'byond.dmi.dmi.DMI', origImage: Image) -> bytes:
        w = cls()
        return cls.GenMetaFromText(w.dump(dmi).encode('ascii'), origImage)

    @classmethod
    def GenMetaFromText(cls, text: bytes, origImage: Optional[Image] = None) -> PngImagePlugin.PngInfo:

        # More borrowed from DMIDE:
        # undocumented class
        meta = PngImagePlugin.PngInfo()

        if origImage:
            # copy metadata into new object
            reserved = ('interlace', 'gamma', 'dpi', 'transparency', 'aspect')
            for k, v in origImage.info.items():
                if k in reserved:
                    continue
                meta.add_text(k, v, 1)

        # Only need one - Rob
        meta.add_text('Description', text, 1)
        return meta

    @classmethod
    def SetRawMetadataIn(cls, text: bytes, infile: Union[str, Path, BinaryIO], outfile: Union[str, Path, BinaryIO]) -> None:
        img = PIL.Image.open(infile)
        img.save(outfile, 'PNG', pnginfo=cls.GenMetaFromText(text))


    def writeToImage(self, dmi: 'byond.dmi.dmi.DMI', image: Image, f: Union[str, Path, BinaryIO], oldimage: Optional[Image] = None):
        image.save(f, 'PNG', pnginfo=self.GenMetaFromDMI(dmi, oldimage))
