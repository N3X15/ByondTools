'''
Originally created to make pipe adapter stuff.

We ended up doing this with shifted overlays instead.
'''
import shutil
import sys
from pathlib import Path

from PIL import Image

# We need to see byond
sys.path.append('.')

from byond.directions import EDirection
from byond.dmi import DMI
from byond.dmi.state import EStateDirections

PIPING_LAYER_DEFAULT = 3  # starting value - this is the "central" pipe
PIPING_LAYER_INCREMENT = 1  # how much the smallest step in piping_layer is

PIPING_LAYER_MIN = 1
PIPING_LAYER_MAX = 5

PIPING_LAYER_P_X = 5  # each positive increment of piping_layer changes the pixel_x by this amount
PIPING_LAYER_P_Y = -5  # same, but negative because they form a diagonal

DIR_MIDCONS = {
    str(EDirection.NORTH): (15, 31),
    str(EDirection.SOUTH): (15, 0),
    str(EDirection.EAST):  (31, 15),
    str(EDirection.WEST):  (0,  15),
}

def genFrame(img: Image.Image, dir: EDirection, layer: int) -> None:
    level_x = 15 + (layer - PIPING_LAYER_DEFAULT) * PIPING_LAYER_P_X
    level_y = 15 - (layer - PIPING_LAYER_DEFAULT) * PIPING_LAYER_P_Y

    midcon_pt = DIR_MIDCONS[str(dir)]
    midcon_x = midcon_pt[0]
    midcon_y = midcon_pt[1]
    print("Generating dir={}, layer={}: ({}, {})".format(dir, layer, level_x, level_y))
    #newf = Image.new('RGBA', (32, 32))
    #newp = newf.load()
    newp = img.load()
    for x in range(32):
        for y in range(32):
            if x == midcon_x or y == midcon_y:
                newp[x, y] = (0, 0, 255, 255)
            if x == level_x or y == level_y:
                newp[x, y] = (255, 128, 0, 255)
    return img


def makeDMI():
    OUTFILE = Path('examples') / 'pipeadapter' / 'pipe_adapter.dmi'

    dmi = DMI(32, 32)
    for layer in range(1,6):
        state = dmi.createState(f'layer {layer}', EStateDirections.CARDINAL, 1)
        for diri in range(4):
            d = EDirection(1 << diri)
            genFrame(state.frames[0].directions[d], d, layer)

    # save
    dmi.saveTo(OUTFILE)
    shutil.copy(OUTFILE, OUTFILE.with_suffix('.png'))

if __name__ == '__main__':
    makeDMI()
