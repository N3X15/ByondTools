# byond.dmi.dmi

```{eval-rst}
.. module:: byond.dmi.dmi
```

## Classes

```{eval-rst}
.. autoclass:: byond.dmi.dmi.DMI
    :members:
```