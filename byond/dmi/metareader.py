from byond.dmi.state import DMIState
from os import PathLike
from typing import Iterable, List, Optional, Union
from PIL import Image
from byond.dmi._metaentry import DMIMetaEntry, EDMIMetaEntryType
from byond.dmi.exceptions import DMIEmptyDescriptionError, DMIMissingDescriptionError, DMIInvalidFormatError

#from byond.i18n import getTxnFor #isort: skip
#t,_ = getTxnFor(__name__)

from byond.logging import getLogger

class DMIMetaReader:
    def __init__(self) -> None:
        self.header: DMIMetaEntry = None
        self.allEntries:List[DMIMetaEntry] = []

    def parseString(self, data: str) -> None:
        if data == '':
            raise DMIEmptyDescriptionError()
        self._parseIterable(data.splitlines(keepends=False))

    def parseFile(self, filename: Union[str,PathLike[str]]) -> None:
        img = Image.open(filename)
        self.parseImage(img)

    @staticmethod
    def AssertPngHasDescription(img: Image.Image) -> None:
        if img.format != 'PNG':
            raise DMIInvalidFormatError()
        if 'Description' not in img.info:
            raise DMIMissingDescriptionError()

    @classmethod
    def GetRawMetadataFrom(cls, pngfile: Union[str, Path, BinaryIO, Image.Image]) -> str:
        img: Image.Image
        if isinstance(pngfile, Image):
            img = pngfile
        else:
            img = Image.open(pngfile, 'r', ['PNG'])
        cls.AssertPngHasDescription(img)
        return img.info['Description']

    def parseImage(self, img: Image.Image) -> None:
        self.parseString(self.GetRawMetadataFrom(img))
    
    def _parseIterable(self, iterable: Iterable[str]) -> None:
        currentEntry: Optional[DMIMetaEntry] = None
        def postProcess() -> None:
            nonlocal currentEntry

            if not currentEntry:
                return

            if currentEntry.type == EDMIMetaEntryType.HEADER:
                assert DMIMetaEntry.VERSION == name
                self.header = currentEntry

            self.allEntries.append(currentEntry)

        for line in iterable:
            sline = line.strip()
            if sline == '':
                continue
            if sline == '#BEGIN DMI':
                in_dmi_header = True
                continue
            if sline == '#END DMI':
                in_dmi_header = False
                break
            if in_dmi_header:
                if not line.startswith(DMIMetaEntry.INDENT):
                    postProcess()
                    typName, name = [x.strip() for x in line.split('=', 1)]
                    currentEntry = DMIMetaEntry(EDMIMetaEntryType(typName), name)
                else:
                    k, v = [x.strip() for x in line.split('=', 1)]
                    currentEntry[k]=v
        postProcess()

    def writeToDMI(self, dmi: 'byond.dmi.dmi.DMI') -> None:
        for entry in self.allEntries:
            if entry.type == EDMIMetaEntryType.HEADER:
                dmi.fromDMIEntry(self.header)
            if entry.type == EDMIMetaEntryType.STATE:
                state = DMIState()
                state.fromDMIEntry(entry)
                dmi.addState(state)

