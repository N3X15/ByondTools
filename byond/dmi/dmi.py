from pathlib import Path
from typing import Any, BinaryIO, Dict, FrozenSet, List, Optional, OrderedDict, Union

from byond.dmi._metaentry import DMIMetaEntry, EDMIMetaEntryType
from byond.dmi.exceptions import DMIUnrecognizedVersionError
from byond.dmi.imagereader import DMIImageReader
from byond.dmi.imagewriter import DMIImageWriter
from byond.dmi.metareader import DMIMetaReader
from byond.dmi.metawriter import DMIMetaWriter
from byond.dmi.state import DMIState, EStateDirections
from PIL import Image as Pillow
from PIL.Image import Image

from byond.logging import getLogger  # isort: skip
log = getLogger(__name__)

from byond.i18n import getTxnFor  # isort: skip
t, _ = getTxnFor(__name__)

__ALL__ = ['DMI']

class DMI:
    '''
    A DMI is a collection of DMIStates and global properties.  They are not always logically related.

    All states in a DMI are the same width and height and share a color palette.
    '''
    
    CURRENT_DMI_VERSION: str = '4.0'

    VALID_KEYS: FrozenSet[str] = frozenset(['height', 'width'])

    def __init__(self, height: Optional[int]=None, width: Optional[int]=None) -> None:
        '''Instantiate a new DMI.
        
        :param height:
            Height of each icon in the DMI, in pixels.
        :param width:
            Width of each icon in the DMI, in pixels.
        '''

        '''The schema version of the file itself.
        
        .. note::
            Just in case we run into a v3 file or w/e.
        '''
        self.version: str = self.CURRENT_DMI_VERSION
        
        #: A list of all available DMI states. Includes duplicated states.
        self.states: List[DMIState] = []

        '''Helper variable for finding states.
        
        .. warning::
        
            Duplicated entries WILL overwrite the original. 
            
            *This is NOT an authoritative depiction of DMI internals.* See :py:attr:`states`.
        '''
        self.statesByKey: Dict[str, DMIState] = {}
        
        #: Width of every state, in pixels.
        self.icon_width: int = width or 0
        
        #: Height of every state, in pixels
        self.icon_height: int = height or 0

        self._image_loaded: bool = False

    def asDMIEntry(self) -> DMIMetaEntry:
        '''Creates a DMIMetaEntry for injection into a DMI as a metadata block header.

        :return:
            The completed :py:class:`byond.dmi._metaentry.DMIMetaEntry` for use in :py:class:`byond.dmi.metawriter.MetaWriter`.
        '''

        # oh shit what are you doing
        assert self.icon_width > 0 and self.icon_height > 0

        e = DMIMetaEntry(EDMIMetaEntryType.HEADER, self.CURRENT_DMI_VERSION)

        e.set('width', self.icon_width)
        e.set('height', self.icon_height)
        return e

    def fromDMIEntry(self, e: DMIMetaEntry) -> None:
        '''Read properties from :py:class:`byond.dmi._metaentry.DMIMetaEntry`.  
        
        Used in :py:class:`byond.dmi.metawriter.MetaReader`.
        '''

        #Idiot check
        assert e.type == EDMIMetaEntryType.HEADER

        if e.name == '4.0':
            actual_keys = set(e.values.keys())

            for key in actual_keys:
                if key not in self.VALID_KEYS:
                    log.warning(_('Unexpected DMI metadata key {key!r} in DMI header. This key-value pair will be ignored. Expected keys: {VALID_KEYS!r}').format(
                        key=key, VALID_KEYS=list(self.VALID_KEYS)))
                        
            self.icon_width = e.get('width', None, typ=int, required=True)
            self.icon_height = e.get('height', None, typ=int, required=True)
        else:
            raise DMIUnrecognizedVersionError(e.name)

    def addState(self, state: DMIState, overwrite: bool=False) -> DMIState:
        '''Add a state to the DMI.
        
        .. warning ::
            By default, adding a state with a duplicate name will OVERWRITE
            any existing state with the same name.

            To add duplicate states, set `overwrite` to `True`.
        :return:
            The state added to the DMI.
        '''
        key = state.key
        if overwrite:
            self.removeStateByName(state.name, state.movement)
        self.states.append(state)
        if key not in self.statesByKey:
            self.statesByKey[key] = state
        return state

    def removeState(self, state: DMIState) -> None:
        '''Removes a state from the DMI'''

        if state in self.states:
            self.states.remove(state)
        if state.key in self.statesByKey and self.statesByKey[state.key] == state:
            del self.statesByKey[state.key]

    def createState(self, name: str, nDirs: EStateDirections=EStateDirections.ONE, nFrames: int=1, movement: bool=False) -> DMIState:
        '''Creates a DMI with the given parameters, then allocates frames with correctly-configured PIL images.

        :return:
            The new state.
        '''
        state = DMIState(name, nDirs, nFrames, movement, alloc=False) # Don't alloc twice.
        state.allocFrames((self.icon_width, self.icon_height))
        self.addState(state)
        return state

    def getStateByName(self, name: str, is_movement: bool = False) -> DMIState:
        '''Get state by name and movement flag.
        
        .. warning::
            States can have duplicated names!
        '''
        return self.statesByKey[DMIState.MakeKey(name, is_movement)]

    def removeStateByName(self, name: str, is_movement: bool = False) -> None:
        '''Remove state by name and movement flag.
        
        .. warning::
            This method will remove all matching states!
        '''
        key = DMIState.MakeKey(name, is_movement)
        if key in self.statesByKey:
            del self.statesByKey[key]
        for oldstate in list(self.states):
            if key == oldstate.key:
                self.states.remove(oldstate)

    @classmethod
    def LoadFromFile(cls, filename: Union[str, Path, BinaryIO]) -> 'DMI':
        '''Load DMI from filename, Path, or binary stream.'''
        return cls.LoadFromImage(Pillow.open(filename))
    @classmethod
    def LoadFromBytes(cls, data: bytes) -> 'DMI':
        '''Load DMI from bytestring.'''
        return cls.LoadFromImage(Pillow.open(data))
    @classmethod
    def LoadFromImage(cls, image: Image) -> 'DMI':
        '''Load DMI from PIL image.'''
        mr = DMIMetaReader()
        mr.parseImage(image)

        dmi: 'DMI' = cls()
        mr.writeToDMI(dmi)

        ir = DMIImageReader()
        ir.loadFromImage(image)
        return dmi

    def writeToImage(self, imgwriter: Optional[DMIImageWriter] = None) -> Image:
        '''Create an atlassed PIL image from DMI contents.

        .. warning::
            This image does not contain the required metadata to qualify as a DMI.
            
            See :py:meth:`.saveTo`.
        '''
        imgwriter = imgwriter or DMIImageWriter()
        return imgwriter.writeToImage(self)
    
    def saveTo(self, f: Union[str, Path, BinaryIO], imgwriter: Optional[DMIImageWriter] = None, metawriter: Optional[DMIMetaWriter] = None) -> None:
        '''Create a .dmi on disk from the DMI's content.'''
        metawriter = metawriter or DMIMetaWriter()
        img = self.writeToImage(imgwriter)
        metawriter.writeToImage(self, img, f)

    def serialize(self, states: bool = True) -> Dict[str, Any]:
        data = OrderedDict({
            'type': self.__class__.__name__,
            'version': self.version,
            'size': [self.icon_height, self.icon_width],
        })
        if states:
            data['states'] = [s.serialize(self) for s in self.states]

    def deserialize(self, data: Dict[str, Any]) -> None:
        assert data['type'] == self.__class__.__name__
        self.version = data['version']
        self.icon_height, self.icon_height = data['size']
        self.states = []
        for sdata in data.get('states', []):
            state = DMIState()
            state.deserialize(sdata)
            self.addState(state)

    def extractTo(self, toDir: Union[str, Path]) -> None:
        toDir = Path(toDir)
        toDir.mkdir(parents=True,exist_ok=True)
        metafile = toDir / 'meta.json'
        metafile.write_text(json.dumps(self.serialize(states=False), indent=2))
        for state in self.states:
            safeStateID = state.getSafeName()
            stateDir = toDir / safeStateID
            state.extractTo(stateDir)
