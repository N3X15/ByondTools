from typing import Optional
import math

class Point3:
    def __init__(self, x: Optional[int] = None, y: Optional[int] = None, z: Optional[int] = None) -> None:
        self.x: int = x or 0
        self.y: int = y or 0
        self.z: int = z or 0

    @property
    def xf(self) -> float:
        return float(self.x)
    @property
    def yf(self) -> float:
        return float(self.y)
    @property
    def zf(self) -> float:
        return float(self.z)

    def distance(self, other: 'Point3', ignore_z: bool = False) -> float:
        a = math.pow(self.xf-other.xf, 2)
        a += math.pow(self.yf-other.yf, 2)
        if not ignore_z:
            a += math.pow(self.zf-other.zf, 2)
        return math.sqrt(a)

    def __str__(self) -> str:
        return f'<Point3({self.x},{self.y},{self.z})>'