import sys
from enum import IntFlag
from typing import List, Union
'''
NORTH, SOUTH, EAST, and WEST are just #define statements built in DM.  

They represent 1, 2, 4, and 8, respectively.
'''

class EDirection(IntFlag):
    NORTH = 1
    SOUTH = 2
    EAST = 4
    WEST = 8

    NORTHWEST = NORTH|WEST
    NORTHEAST = NORTH|EAST
    SOUTHEAST = SOUTH|EAST
    SOUTHWEST = SOUTH|WEST

    ALL = NORTH|SOUTH|EAST|WEST
    COUNT = 4
    NONE = 0

    # This section is just for type checkers and docs
    # https://stackoverflow.com/a/56854002
    _ignore_ = ['BIT_COUNTS', 'IMAGE_INDICES']

    '''Number of bits for each direction.'''
    BIT_COUNTS: List[int] = []

    '''Which direction takes which position in a DMI.'''
    IMAGE_INDICES: List['EDirection'] = []

    def asNameList(self) -> List[str]:
        return [x.name for x in self if x.name not in ('NONE', 'COUNT', 'ALL')]

    @classmethod
    def FromNameList(cls, names: List[str]) -> 'EDirection':
        o = cls(0)
        for name in names:
            o |= cls[name]
        return o


# Yes, this is the fastest way to do it.
EDirection.BIT_COUNTS = [bin(v).count('1') for v in range(EDirection.ALL)]

EDirection.IMAGE_INDICES = [
    EDirection.SOUTH,
    EDirection.NORTH,
    EDirection.EAST,
    EDirection.WEST,
    EDirection.SOUTHEAST,
    EDirection.SOUTHWEST,
    EDirection.NORTHEAST,
    EDirection.NORTHWEST
]

def getDirFromName(name: str) -> EDirection:
    return EDirection[name]

def getNameFromDir(_dir: Union[int, EDirection], as_list: bool = False) -> str:
    valI: int
    valE: EDirection
    if isinstance(_dir, int):
        valE = EDirection(_dir)
        valI = _dir
    else:
        valE = _dir
        valI = valE.value
    
    if not as_list:
        count = EDirection.BIT_COUNTS[valE]
        if count == 1:
            return valE.name
        if count == 2:
            return valE.name
    return ', '.join([str(x.name for x in valE)])
