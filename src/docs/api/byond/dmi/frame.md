# byond.dmi.frame

```{eval-rst}
.. module:: byond.dmi.frame
```

## Classes
```{eval-rst}
.. autoclass:: byond.dmi.frame.DMIFrame
    :members:
```