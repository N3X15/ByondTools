from collections import OrderedDict
from typing import Any, Dict, Optional
from byond.directions import EDirection

from PIL.Image import Image

class DMIFrame:
    '''Representation of a frame within a :py:class:`byond.dmi.dmi.DMI`.
    
    Frames keep track of their own index, their directional icons, and their delay.'''
    def __init__(self) -> None:
        #: Frame order in the state
        self.index: int = 0

        #: Directional icons in the frame
        self.directions: Dict[EDirection, Optional[Image]] = {}

        #: Delay in deciseconds
        self.delay: int = 1

    def serialize(self) -> Dict[str, Any]:
        return OrderedDict({
            'delay': self.delay
        })