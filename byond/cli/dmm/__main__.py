#!/usr/bin/env python
"""
dmm - Collection of map tools.

Copyright 2013-2014 Rob "N3X15" Nelson <nexis@7chan.org>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

"""
import sys, argparse, os, re
from byond import ObjectTree, Map, MapRenderFlags
from byond.basetypes import Atom, PropertyFlags
from byond.map.format.dmm import DMMFormat

from byond.cli.dmm.analyze import _register_analyze
from byond.cli.dmm.diff import _register_diff
from byond.cli.dmm.patch import _register_patch
from byond.cli.dmm.split import _register_split
from byond.cli.dmm.transcribe import _register_transcribe

def main():
    dmmt = DMMFormat(None)

    argp = argparse.ArgumentParser(prog='dmm', description='BYOND game engine map editing tools')
    argp.add_argument('--project', type=str, help='Project file.', metavar='project.dme')
    subp = argp.add_subparsers(help='The command you wish to execute')

    _register_analyze(subp)
    _register_diff(subp)
    _register_patch(subp)
    _register_transcribe(subp)
    _register_split(subp)

    args: argparse.Namespace = argp.parse_args()
    if hasattr(args, 'cmd'):
        args.cmd(args, dmmt)
    else:
        argp.print_help()


if __name__ == '__main__':
    main()
