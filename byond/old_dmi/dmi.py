from typing import Optional, Dict, List
import sys, os, glob, string, traceback, fnmatch, math, shutil, collections
from os import PathLike
from pathlib import Path
from PIL import Image, PngImagePlugin
from byond.dmi.state import DMIState

import logging
log = logging.getLogger(__name__)

class DMILoadFlags:
    #: Load metadata only, no image processing.
    NoImages = 1
    #: Do not perform post-processing. (obsolete)
    NoPostProcessing = 2


class DMI(object):
    '''
    Simple class representing a DMI file.
    '''

    def __init__(self, filename: Optional[PathLike] = None) -> None:
        '''
        Create a DMI instance.  Does not perform loading.
        '''
        super().__init__()
        #: Path of the file.
        self.filename: Optional[PathLike] = Path(filename) if filename is not None else None
        #: Version of the file.
        self.version: str = ''
        #: A dictionary of all available DMI states, keyed on state name.
        self.states: Dict[str, DMIState] = collections.OrderedDict()  # {}
        #: Width of every state, in pixels.
        self.icon_width: int = 32
        #: Height of every state, in pixels
        self.icon_height: int = 32

        # TODO: Figure out if I even need this shit.
        self.pixels = None
        self.size = ()
        self.statelist = 'LOLNONE'
        self.max_x = -1
        self.max_y = -1
        self.img = None

    def save(self, to: PathLike, **kwargs):
        '''
        Save DMI to a specified file.

        :param string to:
            File to write to.
        :param **kwargs:
            See below

        KWargs:
         - `sort` (bool): Defaults True, sorts states based on name.
        '''

        # Sanity testing.
        if len(self.states) == 0:
            log.error('Cannot save DMI: No states in DMI.states.')
            return  # Nope.

        # Now build the manifest
        manifest: str = '#BEGIN DMI'
        manifest += '\nversion = 4.0'
        manifest += f'\n\twidth = {self.icon_width}'
        manifest += f'\n\theight = {self.icon_height}'

        frames = []
        fdata = []

        # Sort by name because I'm autistic like that.
        ordered: Dict[str, DMIState] = self.states
        if kwargs.get('sort', True):
            ordered = collections.OrderedDict(sorted(self.states.items()))

        for name, state in ordered.items():
            if len(state.icons) > 0:
                manifest += state.genManifest()
                numIcons = state.numIcons()
                lenIcons = len(state.icons)
                if numIcons != lenIcons:
                    log.warning(f'numIcons={numIcons}, len(icons)={lenIcons} in state {name!r}!')
                # frames += state.icons
                # frames.extend(state.icons)
                for i in range(len(state.icons)):
                    fdata += [f'{state.name}[{i}]']
                    frames += [state.icons[i]]
            else:
                log.warning(f'State {name} has 0 frames.')
        manifest += '\n#END DMI'

        # print(manifest)

        # Next bit borrowed from DMIDE.
        icons_per_row = math.ceil(math.sqrt(len(frames)))
        rows = icons_per_row

        if len(frames) > icons_per_row * rows:
            rows += 1

        sheet = Image.new('RGBA', (int((icons_per_row + 1) * self.icon_width), int(rows * self.icon_height)))

        x = 0
        y = 0
        # for frame in frames:
        # log.debug('per_row={0}, rows={1}, size={2}'.format(icons_per_row,rows,sheet.size))
        for f in range(len(frames)):
            frame = frames[f]
            icon = frame
            if isinstance(frame, str):
                icon = Image.open(frame, 'r')
            box = (x * self.icon_width, y * self.icon_height)
            # log.debug('{0} -> ({1},{2}) {3} {4}'.format(f,x,y,box,fdata[f]))
            sheet.paste(icon, box)
            x += 1
            if x > icons_per_row:
                y += 1
                x = 0

        # More borrowed from DMIDE:
        # undocumented class
        meta = PngImagePlugin.PngInfo()

        # copy metadata into new object
        reserved = ('interlace', 'gamma', 'dpi', 'transparency', 'aspect')
        for k, v in sheet.info.items():
                if k in reserved: continue
                meta.add_text(k, v, 1)

        # Only need one - Rob
        meta.add_text('Description', manifest.encode('ascii'), 1)

        # and save
        sheet.save(to, 'PNG', pnginfo=meta)
        # with open(to+'.txt','w') as f:
        #    f.write(manifest)
        # logging.info('>>> {0} states saved to {1}'.format(len(frames), to))

    def extractTo(self, dest: PathLike, suppress_post_process: bool=False):
        '''
        Extract DMI to a specified directory

        :param string dest:
            Directory to write to
        :param boolean suppress_post_process:
            Disable post processing of image files
        '''
        # print('>>> Loading %s...' % self.filename)
        self.loadAll(suppress_post_process=suppress_post_process)
        # print('>>> Extracting %s...' % self.filename)
        self.extractAllStates(dest, suppress_post_process=suppress_post_process)

    def getFrame(self, state, direction, frame, movement=False):
        '''
        Get Pillow image for the specified state's direction and frame.

        :param string state:
            The name of the state to look for.
        :param int direction:
            The direction to load.
        :param int frame:
            The frame index.
        :param boolean movement:
            Whether the specified state is a movement state.
        '''
        state = DMIState.MakeKey(state, movement=movement)
        if state not in self.states:
            return None
        return self.states[state].getFrame(direction, frame)

    def setFrame(self, state, direction, frame, img, movement=False):
        '''
        Set Pillow image for the specified state's direction and frame.

        :param string state:
            The name of the state to look for.
        :param int direction:
            The direction to load.
        :param int frame:
            The frame index.
        :param PIL.Image img:
            The image to set the frame to.
        :param boolean movement:
            Whether the specified state is a movement state.
        '''
        state = DMIState.MakeKey(state, movement=movement)
        if state not in self.states:
            self.states[state] = DMIState(state)
        return self.states[state].setFrame(direction, frame, img)

    def getHeader(self):
        '''
        Get the raw PNG Description header.
        '''
        img = Image.open(self.filename)
        if('Description' not in img.info):
            raise Exception("DMI Description is not in the information headers!")
        return img.info['Description']

    def setHeader(self, newHeader: str, dest: PathLike):
        '''
        Set the raw PNG Description header.
        '''
        img = Image.open(self.filename)

        # More borrowed from DMIDE:
        # undocumented class
        meta = PngImagePlugin.PngInfo()

        # copy metadata into new object
        reserved = ('interlace', 'gamma', 'dpi', 'transparency', 'aspect', 'icc_profile')
        for k, v in img.info.items():
                if k in reserved:
                    continue
                # print(k, v)
                meta.add_text(k, v, 1)

        # Only need one - Rob
        meta.add_text('Description', newHeader, 1)

        # and save
        img.save(str(dest) + '.tmp', 'PNG', pnginfo=meta)
        os.replace(str(dest) + '.tmp', dest)

    def loadMetadata(self, **kwargs):
        '''
        Load only the file's metadata.
        '''
        kwargs['no_images'] = True
        self.load(**kwargs)

    def loadAll(self, suppress_post_process: bool = False):
        '''
        Load the DMI file.
        '''
        self.load(suppress_post_process)

    def load(self, no_images: bool = False, suppress_post_process: bool = False) -> None:
        self.img = Image.open(self.filename)

        # This is a stupid hack to work around BYOND generating indexed PNGs with unspecified transparency.
        # Uncorrected, this will result in PIL(low) trying to read the colors as alpha.
        if self.img.mode == 'P':
            # If there's no transparency, set it to black.
            if 'transparency' not in self.img.info:
                logging.warn('({0}): Indexed PNG does not specify transparency! Setting black as transparency. self.img.info = {1}'.format(self.filename, repr(self.img.info)))
                self.img.info['transparency'] = 0

        # Always use RGBA, it causes fewer problems.
        if self.img.mode != 'RGBA':
            self.img = self.img.convert('RGBA')

        self.size = self.img.size

        # Sanity
        if 'Description' not in self.img.info:
            raise Exception("DMI Description is not in the information headers!")

        # Load pixels from image
        self.pixels = self.img.load()

        # Load DMI header
        desc = self.img.info['Description']
        """
        version = 4.0
                width = 32
                height = 32
        state = "fire"
                dirs = 4
                frames = 1
        state = "fire2"
                dirs = 1
                frames = 1
        state = "void"
                dirs = 4
                frames = 4
                delay = 2,2,2,2
        state = "void2"
                dirs = 1
                frames = 4
                delay = 2,2,2,2
        """
        state = None
        x = 0
        y = 0
        self.statelist = desc
        ii = 0
        for line in desc.split("\n"):
            line = line.strip()
            if line.startswith("#"):
                continue
            if '=' in line:
                (key, value) = line.split(' = ')
                key = key.strip()
                value = value.strip().replace('"', '')
                if key == 'version':
                    self.version = value
                elif key == 'width':
                    self.icon_width = int(value)
                    self.max_x = self.img.size[0] / self.icon_width
                elif key == 'height':
                    self.icon_height = int(value)
                    self.max_y = self.img.size[1] / self.icon_height
                    # print(('%s: {sz: %s,h: %d, w: %d, m_x: %d, m_y: %d}'%(self.filename,repr(img.size),self.icon_height,self.icon_width,self.max_x,self.max_y)))
                elif key == 'state':
                    if state != None:
                        # print(" + %s" % (state.ToString()))
                        if(self.icon_width == 0 or self.icon_height == 0):
                            if(len(self.states) > 0):
                                raise SystemError("Width and height for each cell are not available.")
                            else:
                                self.icon_width = self.img.size[0]
                                self.max_x = 1
                                self.icon_height = self.img.size[1]
                                self.max_y = 1
                        elif(self.max_x == -1 or self.max_y == -1):
                            self.max_x = self.img.size[0] / self.icon_width
                            self.max_y = self.img.size[1] / self.icon_width
                        for _ in range(state.numIcons()):
                            state.positions += [(x, y)]
                            if not no_images:
                                state.icons += [self.loadIconAt(x, y)]
                            x += 1
                            # print('%s[%d:%d] x=%d, max_x=%d' % (self.filename,ii,i,x,self.max_x))
                            if(x >= self.max_x):
                                x = 0
                                y += 1
                        self.states[state.key()] = state
                        # if not suppress_post_process:
                        #    self.states[state.name].postProcess()
                        ii += 1
                    state = DMIState(value)
                elif key == 'dirs':
                    state.dirs = int(value)
                elif key == 'frames':
                    state.frames = int(value)
                elif key == 'loop':
                    state.loop = int(value)
                elif key == 'rewind':
                    state.rewind = int(value)
                elif key == 'movement':
                    state.movement = int(value)
                elif key == 'delay':
                    state.delay = value.split(',')
                elif key == 'hotspot':
                    state.hotspot = value
                else:
                    log.warning(f'Unknown key {key!r} (value={value!r})!')
                    sys.exit()

        self.states[state.name] = state
        for _ in range(state.numIcons()):
            self.states[state.name].icons += [self.loadIconAt(x, y)]
            x += 1
            if(x >= self.max_x):
                x = 0
                y += 1

    def extractAllStates(self, dest: PathLike, suppress_post_process: bool = False):
        for _, state in self.states.iteritems():
            # state = State()
            for i in range(len(state.positions)):
                x, y = state.positions[i]
                self.extractIconAt(state, dest, x, y, i)

                if not suppress_post_process:
                    self.states[state.name].postProcess()
                #if dest is not None:
                #    outfolder = os.path.join(dest, os.path.basename(self.filename))
                #    nfn = self.filename.replace('.dmi', '.dmih')
                #    valid_chars = "-_.()[] %s%s" % (string.ascii_letters, string.digits)
                #    nfn = ''.join(c for c in nfn if c in valid_chars)
                #    nfn = os.path.join(outfolder, nfn)
                #    with open(nfn, 'w') as dmih:
                #        dmih.write(self.getDMIH())

    def loadIconAt(self, sx: int, sy: int) -> Image.Image:
        if self.icon_width == 0 or self.icon_height == 0:
            raise SystemError('Image is {}x{}, an invalid size.'.format(self.icon_height, self.icon_width))
        # print("  X (%d,%d)"%(sx*self.icon_width,sy*self.icon_height))
        icon = Image.new(self.img.mode, (self.icon_width, self.icon_height))

        newpix = icon.load()
        for y in range(self.icon_height):
            for x in range(self.icon_width):
                _x = x + (sx * self.icon_width)
                _y = y + (sy * self.icon_height)
                try:
                    pixel = self.pixels[_x, _y]
                    if pixel[3] == 0:
                        continue
                    newpix[x, y] = pixel
                except IndexError:
                    log.error("!!! Received IndexError in %s <%d,%d> = <%d,%d> + (<%d,%d> * <%d,%d>), max=<%d,%d> halting." % (self.filename, _x, _y, x, y, sx, sy, self.icon_width, self.icon_height, self.max_x, self.max_y))
                    log.error('%s: {sz: %s,h: %d, w: %d, m_x: %d, m_y: %d}' % (self.filename, repr(self.img.size), self.icon_height, self.icon_width, self.max_x, self.max_y))
                    log.error('# of cells: %d' % len(self.states))
                    log.error('Image h/w: %s' % repr(self.size))
                    log.error('--STATES:--')
                    log.error(self.statelist)
                    sys.exit(1)
        return icon

    ICON_VALID_CHARS = "-_.()[] %s%s" % (string.ascii_letters, string.digits)
    def extractIconAt(self, state: DMIState, dest: Path, sx: int, sy: int, i: int=0):
        icon = self.loadIconAt(sx, sy)
        outfolder = dest / os.path.basename(self.filename)
        if not outfolder.is_dir():
            os.makedirs(outfolder)
        nfn = f'{state.name}[{i}].png'

        nfn: Path = outfolder / ''.join(c for c in nfn if c in self.ICON_VALID_CHARS)
        if nfn.is_file():
            os.remove(nfn)
        try:
            icon.save(nfn)
        except SystemError as e:
            log.critical("Received SystemError, halting: %s" % traceback.format_exc(e))
            log.critical('{ih=%d,iw=%d,state=%s,dest=%s,sx=%d,sy=%d,i=%d}' % (self.icon_height, self.icon_width, state.ToString(), dest, sx, sy, i))
            sys.exit(1)
        return nfn
