# byond.dmi.exceptions

## Exceptions


### exception byond.dmi.exceptions.DMIMissingDescriptionError()
thrown when a PNG is missing its Description metadata.


### exception byond.dmi.exceptions.DMIInvalidFormatError()
Thrown when a given file is not a PNG.


### exception byond.dmi.exceptions.DMIEmptyDescriptionError()
Thrown when the PNG description field is an empty string.


### exception byond.dmi.exceptions.DMIUnhandledTypeError(typ: str)
Thrown when attempting to serialize a type that isn’t handled by set().serialize().


### exception byond.dmi.exceptions.DMIMissingPropertyError(name: str, typ: Any, key: str)
Thrown from when a required property is missing from an entry.


### exception byond.dmi.exceptions.DMIUnrecognizedVersionError(version: str)
Thrown when the DMI metadata version is not recognized by the reader.
