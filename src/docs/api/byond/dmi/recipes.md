# DMI Recipes

## Open From Disk

```{code-block} python

from byond.dmi import DMI

dmi: DMI = DMI.LoadFromFile('filename.dmi')
# OR
from pathlib import Path
dmi: DMI = DMI.LoadFromFile(Path('path') / 'to' / 'filename.dmi')
# OR
dmi: DMI
with open('filename.dmi', 'rb') as handle:
    dmi = DMI.LoadFromFile(handle)
```

## Getting a state by name

```{code-block} python

from byond.dmi import DMI, DMIState

# Load your DMI
dmi: DMI = DMI.LoadFromFile('my.dmi')

state: DMIState = dmi.getStateByName('state name', movement=False)
```

## Getting a frame

You need to know the direction of the particular frame you want.

```{code-block} python
import PIL
from byond.dmi import DMI, DMIState, DMIFrame
from byond.directions import EDirection

# Load your DMI.
dmi: DMI = DMI.LoadFromFile('my.dmi')

# Obtain your state.
state: DMIState = dmi.getStateByName('state name', movement=False)

# Get the PIL image for frame #1 (index 0), facing south.
image: PIL.Image.Image = state.frames[0].direction[EDirection.SOUTH]
```

You can edit image in-place, if you want. See [Pillow's docs](https://pillow.readthedocs.io/en/stable/handbook/index.html).

## Saving to disk

```{code-block} python

from byond.dmi import DMI

dmi: DMI = DMI.LoadFromFile('filename.dmi')

# Do stuff...

dmi.saveTo('filename.dmi')
```

### Saving to Bytes

```{code-block} python
from io import BytesIO

# Make our bytestring
buf: bytes = b''

# Create our memory buffer in a contextual way
with BytesIO() as f:

    # Write to the memory buffer
    dmi.saveTo(f)

    # Dump the memory buffer to the bytestring.
    buf = f.getvalue()

# buf now has your data in it.
```