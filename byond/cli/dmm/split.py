
import sys, argparse, os, re
from byond import ObjectTree, Map, MapRenderFlags
from byond.basetypes import Atom, PropertyFlags
from byond.map.format.dmm import DMMFormat

def _register_split(subp: argparse.ArgumentParser) -> None:
    _split = subp.add_parser('split', help='Split up a map by z-level.')
    #_split.add_argument('-i','--isolate', help='Isolate a given z-level', metavar='NUM')
    _split.add_argument('map', type=str, help='Map to split.', metavar='map.dmm')
    _split.set_defaults(cmd=cmd_split)

def cmd_split(args: argparse.ArgumentParser) -> None:
    if not os.path.isfile(args.subject):
        print('File {0} does not exist.'.format(args.subject))
        sys.exit(1)
    if not os.path.isfile(args.project):
        print('DM Environment File {0} does not exist.'.format(args.project))
        sys.exit(1)

    dmm = Map(forgiving_atom_lookups=True)
    basename,ext = os.path.splitext(args.subject)
    outfile='{0}.trans{1}'.format(basename,ext)
    dmm.Load(args.subject, format='dmm')
    dmm.Save(outfile, format='dmm', serialize_cleanly=True)
