#!/usr/bin/env python
"""
dmi - Makes merging and splitting sprites a hell of a lot easier.

Copyright 2013-2021 Rob "N3X15" Nelson <nexis@7chan.org>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

"""
import argparse
import shutil
from pathlib import Path

from byond.dmi import DMI
from byond.dmi.metareader import DMIMetaReader
from byond.dmi.metawriter import DMIMetaWriter
from byond.dmi.utils import compare, compare_all

from byond.i18n import getTxnFor #isort: skip
t, _ = getTxnFor(__name__)

from byond.logging import getLogger #isort: skip
log = getLogger(__name__)

def main():
    argp = argparse.ArgumentParser()  # version='0.1')
    subp = argp.add_subparsers(help=_('The command you wish to execute'))

    _register_compare_all(subp)
    _register_compare(subp)
    _register_disassemble_all(subp)
    _register_disassemble(subp)
    _register_get_dmi_data(subp)
    _register_set_dmi_data(subp)

    args = argp.parse_args()
    if hasattr(args, 'cmd'):
        args.cmd(args)
    else:
        argp.print_help()

def _register_disassemble(subp: argparse.ArgumentParser) -> None:
    _disassemble = subp.add_parser('disassemble', help=_('Disassemble a single DMI file to a destination directory'))
    _disassemble.add_argument('file', type=str, help=_('The DMI file to disassemble.'), metavar='file.dmi')
    _disassemble.add_argument('destination', type=str, help=_('The directory in which to dump the resulting images.'), metavar='dest/')
    _disassemble.set_defaults(cmd=cmd_disassemble)

def cmd_disassemble(args: argparse.Namespace) -> None:
    dmi = DMI.LoadFromFile(args.file)
    dmi.extractTo(args.destination)

def _register_disassemble_all(subp: argparse.ArgumentParser) -> None:
    _disassemble_all = subp.add_parser('disassemble-all', help=_('Disassemble a directory of DMI files to a destination directory'))
    _disassemble_all.add_argument('source', type=str, help=_('The DMI files to disassemble.'), metavar='source/')
    _disassemble_all.add_argument('destination', type=str, help=_('The directory in which to dump the resulting images.'), metavar='dest/')
    _disassemble_all.set_defaults(cmd=cmd_disassemble_all)

def cmd_disassemble_all(args: argparse.Namespace) -> None:
    for entry in Path(args.source).rglob('*.dmi'):
        if entry.suffixes[-2] == '.new':
            continue
        toDir = Path(args.destination) / entry.name
        if toDir.is_dir():
            shutil.rmtree(toDir)
        toDir.mkdir(parents=True, exist_ok=True)
        log.info(_('Extracting {entry} to {toDir}...').format(entry=entry, toDir=toDir))
        dmi = DMI.LoadFromFile(entry)
        dmi.extractTo(toDir)

def _register_compare(subp: argparse.ArgumentParser) -> None:
    _compare = subp.add_parser('compare', help=_('Compare two DMI files and note the differences'))
    _compare.add_argument('theirs', type=str, help=_('One side of the difference'), metavar='theirs.dmi')
    _compare.add_argument('mine', type=str, help=_('The other side.'), metavar='mine.dmi')
    _compare.set_defaults(cmd=cmd_compare)

def cmd_compare(args: argparse.Namespace) -> None:
    print(compare(args.theirs, args.mine))

def _register_compare_all(subp: argparse.ArgumentParser) -> None:
    _compare_all = subp.add_parser('compare-all', help=_('Compare two DMI file directories and note the differences'))
    _compare_all.add_argument('theirs', type=str, help=_('One side of the difference'), metavar='theirs/')
    _compare_all.add_argument('mine', type=str, help=_('The other side.'), metavar='mine/')
    _compare_all.add_argument('report', type=str, help=_('The file the report is saved to'), metavar='report.txt')
    _compare_all.set_defaults(cmd=cmd_compare_all)

def cmd_compare_all(args: argparse.Namespace) -> None:
    compare_all(args.theirs, args.mine, args.report, args)

def _register_get_dmi_data(subp: argparse.ArgumentParser) -> None:
    _get_dmi_data = subp.add_parser('get-dmi-data', help=_('Extract DMI header'))
    _get_dmi_data.add_argument('file', type=str, help=_('DMI file'), metavar='file.dmi')
    _get_dmi_data.add_argument('dest', type=str, help=_('The file where the DMI header will be saved'), metavar='dest.txt')
    _get_dmi_data.set_defaults(cmd=cmd_get_dmi_data)

def cmd_get_dmi_data(args: argparse.Namespace) -> None:
    metadata: str = DMIMetaReader.GetRawMetadataFrom(args.file)
    with open(args.dest, 'w') as f:
        f.write(metadata)

def _register_set_dmi_data(subp: argparse.ArgumentParser) -> None:
    _set_dmi_data = subp.add_parser('set-dmi-data', help=_('Set DMI header'))
    _set_dmi_data.add_argument('inputimgfile', type=str, help=_('Input image'), metavar='infile.dmi')
    _set_dmi_data.add_argument('metadata', type=str, help=_('DMI header file'), metavar='metadata.txt')
    _set_dmi_data.add_argument('outputdmifile', type=str, help=_('Output file'), metavar='outfile.dmi')
    _set_dmi_data.set_defaults(cmd=cmd_set_dmi_data)

def cmd_set_dmi_data(args: argparse.Namespace) -> None:
    mf = Path(args.metadata)
    inf = Path(args.inputimgfile)
    outf = Path(args.outputimgfile)
    
    assert inf.is_file(), _('Input image file is missing')
    assert mf.is_file(), _('Metadata file is missing')

    metatext = mf.read_bytes()
    DMIMetaWriter.SetRawMetadataIn(metatext, inf, outf)

if __name__ == '__main__':
    main()
