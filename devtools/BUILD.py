from pathlib import Path
import os
from typing import List

from babel.messages.frontend import CommandLineInterface as Babel
from buildtools import os_utils
from buildtools.maestro import BuildMaestro
from buildtools.maestro.base_target import SingleBuildTarget
from buildtools.maestro.shell import CommandBuildTarget

AVAILABLE_LANGUAGES = ['en_US']
LANGFILES: List[str] = []
LOCALE_DIR = Path('locale')

PYTHON = os_utils.assertWhich('python')

class BabelCompileTarget(SingleBuildTarget):
    BT_LABEL = 'BABEL'

    def __init__(self, outputfile: Path, inputfile: Path, dependencies: List[str] = []):
        self.inputfile: Path = inputfile
        self.outputfile: Path = outputfile
        super().__init__(target=str(outputfile), files=[
            str(inputfile)], dependencies=dependencies)

    def build(self):
        Babel().run(['pybabel', '-q', 'compile', '-i', self.inputfile, '-o', self.outputfile])


def buildDomain(bm: BuildMaestro, domain: str) -> None:
    global LANGFILES
    for lang in AVAILABLE_LANGUAGES:
        outf = LOCALE_DIR / lang / 'LC_MESSAGES' / f'{domain}.mo'
        inf = LOCALE_DIR / lang / 'LC_MESSAGES' / f'{domain}.po'
        LANGFILES.append(bm.add(BabelCompileTarget(outf, inf)).target)


bm = BuildMaestro()

# gettext stuff.
buildDomain(bm, 'byond.cli.dmi')
buildDomain(bm, 'byond.dmi.dmi')
buildDomain(bm, 'byond.dmi.exceptions')
buildDomain(bm, 'byond.dmi.imagereader')
buildDomain(bm, 'byond.dmi.metareader')
buildDomain(bm, 'byond.dmi.state')
buildDomain(bm, 'byond.logging')


SRC_DIR = Path('src')
SRC_DOC_DIR = SRC_DIR / 'docs'
DOC_DIR = Path('docs')
DOC_INDEX = str(DOC_DIR / 'index.md')
SRC_FILES = list([str(x) for x in SRC_DOC_DIR.rglob('*.md')])
os.environ['PYTHONPATH'] = '.'
bm.add(CommandBuildTarget([DOC_INDEX], SRC_FILES, ['sphinx-build', '-b', 'markdown', '-q', '-N', str(SRC_DOC_DIR), str(DOC_DIR)]))

DOC_HTML_DIR = Path('docs-html')
DOC_HTML_INDEX = str(DOC_HTML_DIR / 'index.html')
bm.add(CommandBuildTarget([DOC_HTML_INDEX], SRC_FILES, ['sphinx-build', '-b', 'html', '-q', '-N', str(SRC_DOC_DIR), str(DOC_HTML_DIR)]))

SRC_GRAMMAR_DIR = SRC_DIR / "grammars"
MAP_MODULE_DIR = Path('byond') / "map"
DMM_GRAMMAR_FILE = str(SRC_GRAMMAR_DIR / "dmm.lark")
DMM_PARSER_FILE = str(MAP_MODULE_DIR / 'dmm' / "parser.py")
# -m lark.tools.standalone -s start -o byond\map\dmm\parser.py -c src\grammars\dmm.lark
bm.add(CommandBuildTarget([DMM_PARSER_FILE], [DMM_GRAMMAR_FILE], [
       PYTHON, '-m', 'lark.tools.standalone', 
               '-s', 'start', 
               '-o', DMM_PARSER_FILE, 
               '-c', 
               DMM_GRAMMAR_FILE]))

bm.as_app()
