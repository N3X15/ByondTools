'''
Made the snowfall sprites on /vg/station once upon a time.

Takes two given PNGs and scrolls them at varying rates as 
layers to give a semi-realistic and smooth snowfall effect.

$ python examples/snow/gen_snow.py
'''

import sys
from pathlib import Path
from typing import Callable, List, Tuple

import numpy as np
import numpy.typing as npt
from PIL import Image, ImageDraw, ImageFont

# We need to see the current project.
sys.path.append('.')

from byond.directions import EDirection
from byond.dmi import DMI, DMIState
from byond.dmi.state import EStateDirections

PILPixelArray = npt.NDArray[np.uint8]


def np_from_img(fname: str) -> PILPixelArray:
    return np.asarray(Image.open(fname), dtype=np.float32)


def save_as_img(ar: PILPixelArray, fname: str) -> None:
    Image.fromarray(ar.round().astype(np.uint8)).save(fname)


def norm(ar: PILPixelArray) -> PILPixelArray:
    return 255. * np.absolute(ar) / np.max(ar)


def shift(oldf: PILPixelArray, newf: PILPixelArray, x: int, y: int, size: Tuple[int, int], dir: EDirection, iters=1):
    ox, oy = x, y

    for _ in range(iters):
        if dir & EDirection.SOUTH:
            y += 1
        if dir & EDirection.NORTH:
            y -= 1
        if dir & EDirection.WEST:
            x -= 1
        if dir & EDirection.EAST:
            x += 1
        # Barrel shift
        if x >= size[0]:
            x = 0
        elif x < 0:
            x = size[0]-1
        if y >= size[1]:
            y = 0
        elif y < 0:
            y = size[1]-1

    newf[x, y] = oldf[ox, oy]


def layer1_op(oldf: PILPixelArray, newf: PILPixelArray, x: int, y: int, size: Tuple[int, int]) -> None:
    shift(oldf, newf, x, y, size, EDirection.SOUTH | EDirection.WEST, 1)


def layer2_op(oldf: PILPixelArray, newf: PILPixelArray, x: int, y: int, size: Tuple[int, int]) -> None:
    shift(oldf, newf, x, y, size, EDirection.SOUTH | EDirection.WEST, 2)


def animate_layer(initial_frame: Image.Image, op: Callable[[PILPixelArray, PILPixelArray, int, int, Tuple[int, int]], None], nframes: int) -> List[Image.Image]:
    '''
    :param initial_frame Image:
        Initial frame of the animation.
    :param op callback:
    :param nframes int: 
        Number of frames to generate
    '''
    if initial_frame.mode != 'RGBA':
        image = initial_frame.convert('RGBA')
    frames = []
    for i in range(nframes):
        if i == 0:
            frames += [initial_frame]
        else:
            oldf = frames[i-1]
            oldp = oldf.load()
            newf = Image.new('RGBA', oldf.size)
            newp = newf.load()
            for x in range(oldf.size[0]):
                for y in range(oldf.size[1]):
                    op(oldp, newp, x, y, oldf.size)
            frames += [newf]
    return frames


def makeDMI() -> None:
    dmi = DMI()
    dmi.icon_height = 32
    dmi.icon_width = 32

    EXAMPLE_DIR = Path('examples') / 'snow'

    # LAYER 1
    state = DMIState()
    state.name = 'snowlayer1'
    state.directionCount = EStateDirections.ONE
    state.frameCount = 31
    state.allocFrames()
    for i, f in enumerate(animate_layer(Image.open(EXAMPLE_DIR / 'snow1.png'), layer1_op, 31)):
        state.frames[i].directions[EDirection.SOUTH] = f
    # Add state to DMI
    dmi.addState(state)

    # LAYER 2
    state = DMIState()
    state.name = 'snowlayer1'
    state.directionCount = EStateDirections.ONE
    state.frameCount = 15
    state.allocFrames()
    for i, f in enumerate(animate_layer(Image.open(EXAMPLE_DIR / 'snow2.png'), layer2_op, 15)):
        state.frames[i].directions[EDirection.SOUTH] = f
    # Add state to DMI
    dmi.addState(state)

    # save
    dmi.saveTo(EXAMPLE_DIR / 'snowfx.dmi')
    # print(dmi.states)


if __name__ == '__main__':
    makeDMI()
