import hashlib, ast, os, time, sys
import operator as op
from functools import reduce

from byond.logging import getLogger #isort: skip
log = getLogger(__name__)

from byond.i18n import getTxnFor #isort: skip
t, _ = getTxnFor(__name__)

def md5sum(filename):
    with open(filename, mode='rb') as f:
        d = hashlib.md5()
        while True:
            buf = f.read(4096)  # 128 is smaller than the typical filesystem block
            if not buf:
                break
            d.update(buf)
        return d.hexdigest().upper()

# supported operators
operators = {ast.Add: op.add, ast.Sub: op.sub, ast.Mult: op.mul,
             ast.Div: op.truediv, ast.Pow: op.pow, ast.BitXor: op.xor, ast.BitOr: op.or_,
             ast.Invert: op.inv, ast.LShift: op.lshift, ast.RShift: op.rshift, ast.Mod: op.mod}

def eval_expr(expr):
    """
    >>> eval_expr('2^6')
    4
    >>> eval_expr('2**6')
    64
    >>> eval_expr('1 + 2*3**(4^5) / (6 + -7)')
    -5.0
    """
    #print(dir(ast))
    return eval_(ast.parse(expr).body[0].value)  # Module(body=[Expr(value=...)])

def getElapsed(start):
    return '%d:%02d:%02d.%03d' % reduce(lambda ll, b : divmod(ll[0], b) + ll[1:], [((time.time() - start) * 1000,), 1000, 60, 60])

def secondsToStr(t):
    return "%d:%02d:%02d.%03d" % \
        reduce(lambda ll, b : divmod(ll[0], b) + ll[1:], [(t * 1000,), 1000, 60, 60])

class TimeExecution(object):
    def __init__(self, label):
        self.start_time = None
        self.label = label

    def __enter__(self):
        self.start_time = time.time()
        return self

    def __exit__(self, type, value, traceback):
        log.info(_('Completed in {seconds}s - {label}').format(label=self.label, seconds=secondsToStr(time.time() - self.start_time)))
        return False

class ProfilingTarget:
    def __init__(self, name):
        self.name = name
        self.calls = 0
        self.elapsed = 0
        self.start_time = 0

    def start(self):
        self.start_time = time.time()

    def end(self):
        el = time.time() - self.start_time
        self.calls += 1
        self.elapsed += el
        return el

    def __str__(self):
        return _("{name} - C: {calls}, E: {elapsed}, A: {avg}").format(name=self.name, calls=self.calls, elapsed=getElapsed(self.elapsed), avg=getElapsed(self.elapsed / self.calls))

    def ToCSV(self):
        return ','.join([self.name, self.calls, getElapsed(self.elapsed), getElapsed(self.elapsed / self.calls)])

class Profiler:
    def __init__(self):
        self.targets = {}

def eval_(node):
    if isinstance(node, ast.Num):  # <number>
        return node.n
    elif isinstance(node, ast.operator):  # <operator>
        return operators[type(node)]
    elif isinstance(node, ast.BinOp):  # <left> <operator> <right>
        return eval_(node.op)(eval_(node.left), eval_(node.right))
    else:
        raise TypeError(node)


_ROOT = os.path.abspath(os.path.dirname(__file__))

def get_data(path):
    return os.path.join(_ROOT, 'data', path)

def get_stdlib(path=''):
    if path != '':
        return os.path.join(get_data('stdlib'), path)
    return get_data('stdlib')

'''
try:
    from line_profiler import LineProfiler

    def do_profile(follow=[]):
        def inner(func):
            def profiled_func(*args, **kwargs):
                try:
                    profiler = LineProfiler()
                    profiler.add_function(func)
                    for f in follow:
                        profiler.add_function(f)
                    profiler.enable_by_count()
                    return func(*args, **kwargs)
                finally:
                    profiler.print_stats()
            return profiled_func
        return inner

except ImportError:
    def do_profile(follow=[]):
        "Helpful if you accidentally leave in production!"
        def inner(func):
            def nothing(*args, **kwargs):
                return func(*args, **kwargs)
            return nothing
        return inner
'''

def GetFilesFromDME(dmefile='baystation12.dme', ext='.dm'):
    filesInDME = []
    rootdir = os.path.dirname(dmefile)
    with open(dmefile, 'r') as dmeh:
        for line in dmeh:
            if line.startswith('#include'):
                inString = False
                # escaped=False
                filename = ''
                for c in line:
                    """
                    if c == '\\' and not escaped:
                        escaped = True
                        continue
                    if escaped:
                        if
                        escaped = False
                        continue
                    """
                    if c == '"':
                        inString = not inString
                        if not inString:
                            filepath = os.path.join(
                                rootdir, filename.replace('\\', os.sep))
                            if filepath.endswith(ext):
                                filesInDME += [filepath]
                            filename = ''
                        continue
                    else:
                        if inString:
                            filename += c
    return filesInDME
