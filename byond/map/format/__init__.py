import glob, os
from .base import *
import logging
_log = logging.getLogger(__name__)
_MAPFORMATS_LOADED: bool = False
def LoadMapFormats():
    global _MAPFORMATS_LOADED
    if _MAPFORMATS_LOADED:
        return
    print('Loading Map Format Modules...')
    for f in glob.glob(os.path.dirname(__file__) + "/*.py"):
        modName = 'byond.map.format.' + os.path.basename(f)[:-3]
        print(' Loading module ' + modName)
        mod = __import__(modName)
        for attr in dir(mod):
            if not attr.startswith('_'):
                #print('  {} = {}'.format(attr,getattr(mod, attr)))
                globals()[attr] = getattr(mod, attr)
    _MAPFORMATS_LOADED = True

def GetMapFormat(_map,ext):
    if not _MAPFORMATS_LOADED:
        LoadMapFormats()
    f = MapFormat.all.get(ext.strip('.'),None)
    if f is None:
        _log.error('Unable to find MapFormat for {}.'.format(ext))
    return f(_map)
