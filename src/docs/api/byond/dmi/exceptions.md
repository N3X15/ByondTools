# byond.dmi.exceptions

```{eval-rst}
.. module:: byond.dmi.exceptions
```

## Exceptions
```{eval-rst}
.. autoexception:: byond.dmi.exceptions.DMIMissingDescriptionError
    :members:

.. autoexception:: byond.dmi.exceptions.DMIInvalidFormatError
    :members:

.. autoexception:: byond.dmi.exceptions.DMIEmptyDescriptionError
    :members:

.. autoexception:: byond.dmi.exceptions.DMIUnhandledTypeError
    :members:

.. autoexception:: byond.dmi.exceptions.DMIMissingPropertyError
    :members:

.. autoexception:: byond.dmi.exceptions.DMIUnrecognizedVersionError
    :members:
```