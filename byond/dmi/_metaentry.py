from enum import Enum
from typing import Any, Dict, Optional

from byond.dmi.exceptions import DMIMissingPropertyError, DMIUnhandledTypeError

class EDMIMetaEntryType(Enum):
    UNKNOWN = ''
    HEADER = 'version'
    STATE = 'state'

class DMIMetaEntry:
    '''
    Represents an object in a DMI's header.
    '''
    LINEBREAK = '\n'
    INDENT = '\t'
    CURRENT_VERSION = '4.0'
    def __init__(self, typ: Optional[EDMIMetaEntryType] = None, name: Optional[str] = '') -> None:
        self.type: EDMIMetaEntryType = typ or EDMIMetaEntryType.UNKNOWN
        self.name: str = name or ''
        self.values: Dict[str, Any] = {}

    def dump(self) -> str:
        o = f'{self.LINEBREAK}{self.type.value}={self.name}'
        for k,v in self.values.items():
            o += f'{self.LINEBREAK}{self.INDENT}{k}={v}'
        return o

    def set(self, key: str, value: Any, default: Any = None) -> None:
        def serialize(value: Any) -> str:
            if isinstance(value, bool):
                return '1' if value else '0'
            elif isinstance(value, list):
                return ','.join([serialize(v) for v in value])
            elif isinstance(value, (int, float)):
                return str(value)
            elif isinstance(value, str):
                return value
            else:
                raise DMIUnhandledTypeError(type(value))
                
        if value == default:
            return

        self.values[key] = serialize(value)

    def get(self, key: str, default: Optional[Any] = None, typ: Any = str, from_list: bool=False, required: bool=False) -> Any:

        def deserialize(value: str, typ: Optional[Any]) -> None:
            if typ == bool:
                return value == '1'
            return typ(value)

        if not self.has(key):
            if required:
                raise DMIMissingPropertyError(self.type, key)
            return default
        value: Any = self.values[key]
        if not from_list:
            return deserialize(value, typ)
        else:
            return [deserialize(v, typ) for v in value.split(',')]
