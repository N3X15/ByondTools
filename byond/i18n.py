import sys
from gettext import translation, GNUTranslations
from typing import Callable, List, Tuple
from pathlib import Path

TXN_DIR: Path = Path(__file__).parent.parent / 'locale'
# Keep in sync with BUILD_LOCALE.py
AVAILABLE_LANGUAGES: List[str] = ['en_US']
def getTxnFor(modname) -> Tuple[GNUTranslations, Callable[[str], str]]:
    '''
    Shortcut to obtain t and _ in modules.
    '''
    t = translation(modname, localedir=TXN_DIR, languages=AVAILABLE_LANGUAGES)
    _ = t.gettext
    return (t, _)