# BYOND DMI API

BYOND stores images in an atlased PNG with embedded metadata called a DMI (DreamMaker Icon).

This API lets you mess with DMI using the v4.0 metadata format. More formats will become available over time.

```{toctree}
:hidden: true
recipes.md
dmi.md
exceptions.md
frame.md
state.md
```

## Important Classes
```{eval-rst}
* :py:class:`byond.dmi.dmi.DMI`
* :py:class:`byond.dmi.frame.DMIFrame`
* :py:class:`byond.dmi.state.DMIState`
```

## Important Enums
```{eval-rst}
* :py:class:`byond.dmi.state.EStateDirections`
```

## All Sub-Modules
```{eval-rst}
* :py:mod:`byond.dmi._metaentry` (You shouldn't mess with this!)
* :py:mod:`byond.dmi.dmi`
* :py:mod:`byond.dmi.exceptions`
* :py:mod:`byond.dmi.frame`
* :py:mod:`byond.dmi.imagereader`
* :py:mod:`byond.dmi.imagewriter`
* :py:mod:`byond.dmi.metareader`
* :py:mod:`byond.dmi.metawriter`
* :py:mod:`byond.dmi.state`
```