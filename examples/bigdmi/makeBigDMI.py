#!/usr/bin/env python
'''
Created on Feb 28, 2014 to test a bug in DreamMaker.

This bug has since been fixed, apparently?

Requires the arial TrueType font to be installed on your OS.
'''

import shutil, sys, colorsys
from pathlib import Path
from typing import Optional
from PIL import Image, ImageDraw, ImageFont

# We have to see the byond module.
sys.path.append('.')

from byond.directions import EDirection
from byond.dmi.state import EStateDirections
from byond.dmi import DMI, DMIState

def makeDMI():
    dmi = DMI(32, 32)
    # Set up our font outside of the loop
    font: Optional[ImageFont.FreeTypeFont] = ImageFont.truetype('arial.ttf', 10) 
    for i in range(513):
        # Make a new tile
        r,g,b = colorsys.hsv_to_rgb(float(i)/512., s=.5, v=.5)
        img = Image.new('RGBA', (32, 32), color=(int(r*255),int(g*255),int(b*255)))
        # Set up PIL's drawing stuff
        draw = ImageDraw.Draw(img)
        # Make our text
        txt = str(i+1)
        # Get the size of the text
        w, h = font.getsize(txt)
        # Draw the tile number
        draw.text(((dmi.icon_width-w)/2, (dmi.icon_height-h)/2), str(i + 1), fill=(255,0,0) if i > 511 else (255,255,255), font=font)
        # Make state
        state=DMIState(f'state {i+1}', EStateDirections.ONE, 1)
        # First frame, southern direction (the only one present).
        state.frames[0].directions[EDirection.SOUTH] = img
        # Add state to DMI
        dmi.addState(state)
    #save
    outfile = Path('examples') / 'bigdmi' / 'state_limit.dmi'
    dmi.saveTo(outfile)
    shutil.copy(outfile, outfile.with_suffix('.png'))

if __name__ == '__main__':
    makeDMI()
